\chapter{Výpočetní výkon}
\label{chap:druha}
Základní předpoklad pro výběr nejvhodnějšího zařízení je schopnost jejich porovnání. To lze dělat mnoha způsoby podle toho, které parametry produktu upřednostňujeme. Mohou to být výkon, pořizovací cena, provozní náklady, vzhled, výrobce a další. V oblasti počítačového hardwaru nás zajímají především první tři. Nejproblematičtějším bodem je samotné měření výpočetního výkonu, které závisí na konkrétní architektuře počítače a požadavcích zadavatele. Jako měřící nástroj se používá sada testů (algoritmů nebo počítačových programů), které v~dostatečné míře implementují zadané požadavky. Taková sada se obecně nazývá počítačový benchmark. Výsledky, které vypovídají o výpočetním výkonu, mohou být nejčastěji doba běhu testu, velikost zpracovaných dat nebo latence či propustnost. Tyto výsledky jsou vždy závislé na konkrétním druhu testování a nelze je porovnávat mezi různými benchmarky. Většinou bývá nutné dodatečné statistické zpracování naměřených údajů, případně odfiltrování odlehlých hodnot.

\section{Architektura počítačů}
Architektura dnešních počítačů \cite{architektura-pocitacu} je poměrně složitá a do značné míry různorodá. V této kapitole přiblížíme základní součásti počítače, jejich funkci a vzájemné propojení. Nebudeme se zabývat detailním popisem všech komponent moderních počítačů, ale zaměříme se pouze na činnost částí, které mají největší vliv na celkový výpočetní výkon. Základní schéma propojení jednotlivých komponent je na obr. \ref{fig:computer_architecture}.

\begin{figure}[h]
    \centering
    \includegraphics[width=0.6\textwidth]{./img/SchemaComputerArchitecture.pdf}
    \caption{Schéma architektury počítače.}
    \label{fig:computer_architecture}
\end{figure}

Hlavní částí počítače je procesor. Ten úzce spolupracuje s čipovou sadou, která se skládá z jednoho nebo více integrovaných obvodů, jejichž funkce je zprostředkovávat komunikaci mezi procesorem, sběrnicemi, řadiči a dalšími součástkami. Termín čipová sada obvykle označuje dva čipy -- severní můstek (northbridge) a jižní můstek (southbridge). Obě části spolu komunikují po interní systémové sběrnici. Severní můstek se stará o komunikaci mezi procesorem a operační pamětí, případně i grafickým čipem. Jižní můstek integruje rozšiřující sběrnice (PCIe, SATA, ...), skrze které se připojují periferní zařízení. Nejnovější trend vývoje je umisťování těchto součástek přímo do procesoru.

\subsection{Procesor}
Procesor neboli CPU (Central Processing Unit) je integrovaný obvod provádějící strojové instrukce. Různé procesory mohou zpracovávat různý strojový kód. Množina platných instrukcí pro procesor (jeho instrukční sada) je dána jeho architekturou. Způsob, jakým je v procesoru implementována daná instrukční sada, nazýváme mikroarchitektura. Pro jednu architekturu může existovat více mikroarchitektur, které ji implementují. Mezi architektury procesorů patří například ARMv6KZ (Raspberry Pi) či Intel 64 (počítače se kterými budeme Raspberry Pi porovnávat, jejich použitá mikroarchitektura je Nehalem).

\begin{figure}[h]
    \centering
    \includegraphics[width=0.8\textwidth]{./img/Intel_Nehalem_arch.pdf}
    \caption{Schéma procesoru Intel Core i7. \textit{Zdroj: Wikipedia}}
    \label{fig:intel_i7}
\end{figure}

Na obr. \ref{fig:intel_i7} je schéma procesoru Intel Core i7 Nehalem \cite{intel-nehalem}. Jedná se o jeden z procesorů, na kterých budeme provádět měření. Dnešní procesory Intel s~mikroarchitekturou Haswell se v základních principech fungování neliší.

Procesor může obsahovat jedno nebo více výpočetních jader. Většina procesorů z rodiny Intel Core i7 obsahuje čtyři fyzická jádra podporující Hyper-Threading, díky čemuž se procesor jeví jako osmijádrový. Kvůli tomuto rozdělení je zajištěno lepší využití každého fyzického jádra.

Procesor při svém běhu čte jednotlivé instrukce, které dále zpracovává odpovídajícím způsobem. Většina procesorů pro urychlení výpočtu používá pipelining, díky kterému je v jednom okamžiku rozpracováno více instrukcí, což přináší lepší využití procesorových obvodů.

Rychlost procesoru je nejčastěji udávána pomocí taktovací frekvence. Každá instrukce se provede za předem stanovený počet taktů, ale kvůli různým technikám zvýšení výkonu v dnešních procesorech (například pipelining) již frekvence nemusí být rozhodujícím faktorem výpočetního výkonu pro porovnávání procesorů, přesto jeho rychlost stále významně ovlivňuje výkon celého počítače. Parametry procesoru se nejvíce projeví u výpočetně náročných úloh, které pracují s~omezenou množinou dat.

\subsection{Operační paměť}
Operační paměť se běžně označuje zkratkou RAM (Random Access Memory), která původně označovala jakoukoli paměť s náhodným přístupem k datům. Polovodičové paměti RAM rozdělujeme podle technologie uchovávání informace na statickou (SRAM) a dynamickou (DRAM). Statické paměti jsou velmi rychlé, ale díky své konstrukci také drahé. Používají se např. ve vyrovnávacích pamětech procesorů. Dynamické paměti jsou levnější a jednodušší, ale potřebují po určitém čase obnovovat obsah každé paměťové buňky. Tento typ je používán jako operační paměť v osobních i serverových počítačích.

Operační paměť slouží k uložení kódu programu, vstupních i výstupních dat i průběžných mezivýsledků výpočtu. Protože procesor je výrazně rychlejší než paměť RAM a přímá práce s touto pamětí by ho zdržovala, používají se jako mezistupeň rychlejší asociativní vyrovnávací paměti.

Asociativní vyrovnávací paměť (cache) slouží jako vyrovnávající mezivrstva mezi dvěma subsystémy pro zvýšení celkového výkonu. V procesoru cache zajišťují rychlý přístup k často používaným datům. Představený procesor Intel Core i7 obsahuje tři úrovně vyrovnávacích pamětí -- L1, L2 a L3. Se vzrůstající úrovní paměti roste její kapacita, ale také klesá rychlost. První dvě úrovně má každé jádro vlastní, L3 je sdílená všemi procesorovými jádry. Hierarchie pamětí cache je znázorněna na obr. \ref{fig:intel_i7}. Tyto paměti jsou velmi důležité pro výkon celého počítače, protože omezují čekání procesoru na dokončení operací načítání a ukládání dat.

Pro správu paměti RAM v operačním systému se používá koncept stránkování. Každý běžící proces má vlastní souvislou virtuální paměť, která se podle potřeby mapuje do fyzické operační paměti. Toto mapování se provádí po souvislých částech dané velikosti (typicky 4 kB), které nazýváme paměťové stránky. Převod virtuální adresy na adresu korespondující stránky ve fyzické paměti zajišťuje jednotka MMU (Memory Management Unit), která bývá součástí procesoru. Protože k překladům adres mezi virtuální a fyzickou pamětí dochází velmi často a jedna operace překladu znamená i několik čtení z paměti, existuje v procesoru speciální TLB cache (Translation Lookaside Buffer), která urychluje čtení hodnot ze stránkovací tabulky a tím výrazně zvyšuje rychlost překladu adres.

Parametry paměti RAM mohou u některých úloh výrazně ovlivňovat výkon celého počítače. Důležitá je jak její rychlost, tak také dostatečná kapacita. Rychlost konkrétní aplikace ve velké míře závisí na jejím způsobu práce s daty, především zda lze v plné míře využít procesorové paměti cache.

\subsection{Periferní zařízení}
K interakci s počítačem jsou nezbytnou součástí různá vstupní a výstupní zařízení, souhrnně označovaná jako periferní zařízení. Může se jednat o grafický čip s připojeným monitorem, paměťové médium, tiskárnu, klávesnici a myš, webkameru a mnohá další.

Pro výpočetní cluster z jednotek Raspberry Pi je nejdůležitější persistentní paměťové úložiště a Ethernet. Přístup do paměťového úložiště se používá v každé aplikaci (minimálně při jejím spuštění), síť používá cluster z Raspberry Pi jako komunikační médium. Ostatní periferní zařízení mají v této konfiguraci jen velmi malé uplatnění.

Nejpoužívanější persistentní úložiště pro desktopové a serverové počítače jsou HDD (Hard Disk Drive) a SSD (Solid State Drive) disky. HDD je médium s několika rotačními plotnami, kde pohyblivá hlavička čte či zapisuje magnetickou stopu. Tento typ disků dnes umožňuje uchovávat data v jednotkách terabajtů. Nevýhodou je nízká odolnost při otřesech v průběhu čtení a zápisu. Alternativní SSD disky jsou založeny na technologii flash pamětí, které uchovávají informace pomocí soustavy tranzistorů. Tyto disky dosahují vyšších rychlostí čtení i zápisu, ale jejich pořizovací cena je výrazně vyšší proti klasickým HDD. Testovaný referenční desktopový počítač používá SSD disky a k serverovému počítači je připojeno diskové pole s oběma uvedenými typy disků. Nejčastější připojení těchto periferních zařízení je přes SATA rozhraní do jižního můstku, případně vzdáleně přes počítačovou síť.

Pro mobilní zařízení a jednodeskové počítače včetně Raspberry Pi jsou nejpoužívanější SD karty (případně microSD), které obsahují flash paměť podobně jako SSD disky. Hlavní předností proti klasickým HDD diskům je jejich malá fyzická velikost, proto se často používají v drobných zařízeních jako jediné paměťové úložiště. S velikostí pak úzce souvisí také jejich řádově nižší kapacita i přenosová rychlost.

Druhé podstatné periferní zařízení pro uzel v clusteru je Ethernet, souhrn technologií pro počítačové sítě. Aktuální standardizovaná verze je schopna přenosové rychlosti až 100 Gbit/s, avšak běžná zařízení podporují rychlosti 100 Mbit/s a 1 Gbit/s. Jako přenosové médium se používají kabely s páry kroucených vodičů nebo optické kabely. Podstatné parametry týkající se výkonu jsou rychlost výměny dat a zpoždění způsobené přenosem. Cluster z jednotek Raspberry Pi bude využívat Ethernet jako komunikační médium z důvodu jeho rozšířenosti a podpory zařízením. Alternativní možnost propojení, která by se vyrovnala výkonu Ethernetu, deska nenabízí.

Grafický čip by mohl být využit pro jisté typy výpočtů, ale bohužel většina jednodeskových zařízení nepodporuje žádné standardní rozhraní (jako OpenCL nebo Nvidia CUDA). V této oblasti je zatím nevyužitý potenciál těchto malých zařízení, protože grafické čipy se svým velkým množstvím výpočetních jader jsou velmi výkonné pro datově-paralelní druhy výpočtů.

\section{Měření počítačového výkonu}
K měření výkonu počítačů \cite{evaluating-and-benchmarking} lze použít počítačový benchmark. Jedná se o experimentální metodu s omezenou přesností, jejímž cílem je relativní porovnání několika počítačů. Měření probíhá nejčastěji tak, že na cílovém počítači spouštíme vybrané algoritmy a zaznamenáváme dobu jejich běhu. Vždy je důležité správně zhodnotit aktuální požadavky a najít vhodné testovací algoritmy a odpovídající vstupní data.

Benchmarky lze dělit do několika skupin podle své funkce, ale neexistuje žádná přesná a všeobecně přijímaná definice takového dělení. Tyto skupiny navíc nejsou vždy disjunktní a přesně vymezené. Některé benchmarky mají více funkcí a proto je můžeme zařadit do více skupin současně. Nejčastěji rozlišujeme tyto typy:

\begin{itemize}
\item aplikační benchmark,
\item mikrobenchmark,
\item kernel benchmark,
\item syntetický benchmark,
\item I/O (input/output, vstupně-výstupní) benchmark.
\end{itemize}

Aplikační benchmark měří výkon reálného programu, mikrobenchmark se zabývá testováním jednotlivých komponent systému, převážně se skládá z relativně malých a specificky zaměřených kusů kódu. Kernel benchmark měří základní architektonické rysy paralelních počítačů. Syntetické benchmarky zohledňují četnosti jednotlivých operací získané z mnoha aplikačních programů a testují výkon počítače na programu napsaném podle získaných četností. I/O benchmarky testují periferní zařízení.

Vhodný benchmark pro celkové změření výkonu jednodeskových počítačů nelze jednoduše zařadit. Měl by testovat jednotlivé komponenty a periferní zařízení počítače odděleně i celkové chování systému na reálně používaných algoritmech.%\\
%TOTO je takovy blby hack, chtelo by to vyresit nejak civilizovane.

\subsection{Vybrané benchmarky na platformě Linux}
Jako operační systém pro Raspberry Pi v zamýšleném použití použijeme některou z distribucí GNU/Linuxu. To je jediný systém, který má dostatečnou podporu většiny malých zařízení a vyhovuje pro použití ve výpočetním clusteru. Proto se v následujícím zaměříme pouze na produkty pro tento operační systém, případně produkty multiplatformní.

\textbf{Phoronix Test Suite} \cite{phoronix-suite} je open-source benchmark, který je poměrně rozšířený a velmi rozsáhlý. Podporuje širokou škálu operačních systémů a architektur procesorů. Obsahuje přes sto testovacích modulů s možností rozšiřování. Celý projekt je napsán v jazyce php a je dobře zdokumentován. Systém umožňuje opakované spouštění testů, nahrávání výsledků na web \textit{OpenBenchmarking.org}, grafické zobrazení naměřených výsledků, sledování relevantních údajů ze systémových logů, sledování sensorů v průběhu testování a další. Výhodou je široká komunita okolo tohoto projektu a finanční podpora ze strany komerčních organizací.
\\\\
\textbf{Bonnie++} \cite{bonnie++} je UNIXový benchmark zaměřený na souborový systém a diskové operace. Vznikl v roce 1999 z původního projektu \textsl{Bonnie} úpravou a přepsáním do jazyka C++. Výstup ukládá do souboru v CSV formátu vhodném ke strojovému zpracování. Vývoj probíhá velmi pomalu.
\\\\
Benchmarky \textbf{SPEC} (od \textsl{Standard Performance Evaluation Corporation}) \cite{spec} jsou velmi známé a populární. Nejznámější je sada zaměřená na výkon procesoru, \textsl{SPEC CPU2006}. Tyto benchmarky se vyznačují vysokou přesností a ve všech verzích se snaží zachovávat stejné standardy. Většina produktů organizace \textsl{SPEC} je dostupná pouze komerčně za ceny od 50~\$ do 5000~\$, proto pro účely této práce nevyhovuje.
\\\\
\textbf{LMBench} \cite{lmbench} je známý a dlouhodobě vyvíjený UNIXový benchmark. \textsl{LMBench} se zabývá měřením latence a propustnosti v několika různých oblastech, například propustnost systémové roury (pipe) či latence přepínání kontextu. \textsl{LMBench} je napsaný v jazyce ANSI C.  
\\\\
\textbf{LINPACK} \cite{linpack} je původně knihovna pro výpočty týkající se lineární algebry napsaná v jazyce Fortran. \textsl{LINPACK benchmark} byl poprvé uveden v roce 1979 jako příloha k uživatelskému manuálu dodávanému ke knihovně. Benchmark je zaměřen na operace s čísly v plovoucí desetinné čárce. Provádí řešení soustavy $n$ lineárních rovnic $Ax=b$. Jeho paralelní verze \textsl{HPL} (\textsl{High Performance LINPACK}) se používá k testování paralelních počítačů a distribuovaných systémů. Výsledky měření \textsl{HPL} jsou mimo jiné použity i k sestavování žebříčku 500 nejlepších superpočítačů planety \cite{top500}. Jsou k dispozici oficiální implementace v jazycích Java, C a Fortran.
\\\\
Každý z výše prezentovaných programů nějakým způsobem pro naše účely nevyhovuje. Některé používají interpretovaný programovací jazyk, nepodporují všechny požadované testy či jsou dostupné pouze pod komerční licencí.

\section{Přehled požadavků}
\label{sec:pozadavky}
Hlavním cílem práce je přinést porovnání výkonu a ceny (pořizovací i provozní) clusteru postaveného z mnoha jednotek jednodeskových počítačů oproti běžně dostupným serverovým a desktopovým počítačům. Motivací je zpracovávání dobře paralelizovatelných úloh v rozumném čase a s co nejmenšími náklady.

V této sekci stanovíme základní parametry, které by měl splňovat vhodný benchmark. Chceme provést testy jednotlivých komponent zvlášť i jejich kombinací na reálně používaných algoritmech. Zajímá nás především procesor, operační paměť, persistentní úložiště a síťový subsystém. Tento výběr plyne z plánovaného použití jednodeskového počítače jako jednotky ve výpočetním clusteru, kde každý uzel bude zapojen v konfiguraci pouze s připojenou sítí a bude se podílet na zpracování výpočetně náročných úloh.

\begin{itemize}
    \item Procesor
        \begin{itemize}
            \item výkon aritmetických operací s celými čísly a čísly s plovoucí desetinnou čárkou
            \item využití procesorových pamětí cache
        \end{itemize}
    \item Paměť RAM
        \begin{itemize}
            \item rychlost čtení a zápisu
            \item rychlost přístupu v závislosti na využití pamětí cache
        \end{itemize}
    \item Persistentní úložiště
        \begin{itemize}
            \item rychlost sekvenčního a náhodného čtení ze souboru a sekvenčního zápisu do souboru
        \end{itemize}
    \item Ethernet
        \begin{itemize}
            \item latence a rychlost TCP i UDP přenosu po různě velkých blocích
        \end{itemize}
\end{itemize}
Z důvodu použití jednojádrového procesoru v počítači Raspberry Pi budou všechny testy testy navrženy jako jednovláknové.\\\\
%\vspace{5 mm}
Dále bychom od implementace měřícího programu chtěli dodržení následujících podmínek.
\begin{itemize}
    \item Benchmark by měl jít spustit na operačním systému GNU/Linux. Důvodem je podpora mnoha typů zařízení včetně většiny jednodeskových počítačů, možnost vlastní úpravy a bezplatná distribuce.
    \item Testovací program by měl podporovat minimálně dvě architektury procesorů, ARM a x86 (IA-32, případně Intel 64). To jsou dnes nejrozšířenější architektury, které pokryjí většinu jednodeskových, serverových i desktopových počítačů.
    \item Benchmark půjde přeložit do nativního kódu dané platformy z důvodu omezených prostředků dnešních jednodeskových počítačů. Jazyky používající správu paměti pomocí \textsl{garbage collectoru} nejsou vhodné, protože programátor nemá možnost sám uvolňovat naalokovanou paměť a spuštění úklidu paměti v nevhodnou dobu může výrazně ovlivnit výsledky měření.
    \item Testovací aplikace by měla pokrýt všechny požadované testy a měla by zachovávat jednotné uživatelské rozhraní, které usnadní orientaci v programu a především strojové zpracovávání výsledků. Vzhledem k předpokládanému použití programu počítačově zdatnými uživateli je konzolové uživatelské rozhraní naprosto dostatečné.
    \item Benchmark by měl být k dispozici zdarma včetně zdrojových kódů jako open-source, aby bylo možné ho udržovat a dále rozšiřovat.
\end{itemize}

Z výše uvedených požadavků vyplývá, že žádný z~dříve zmiňovaných benchmarků není plně vyhovující pro naše měření. \textsl{Phoronix Test Suite} je nevyhovující z důvodu své přílišné rozsáhlosti a použití interpretovaného jazyka php. \textsl{Bonnie++} je příliš úzce zaměřený a nepodporuje všechny námi požadované testy, rozšiřování o další moduly není možné. Benchmarky \textsl{SPEC} nevyhovují svým zaměřením pouze na výkon procesoru a komerční licencí. \textsl{LMBench} se zabývá pouze měřením latence a propustnosti, chybí například všechny testy na měření výkonu procesoru. \textsl{LINPACK benchmark} se zabývá nejdůležitější oblastí z pohledu paralelního programování. Tento benchmark je nejblíže požadavkům naší práce, ale přesto není vhodný z důvodu absence testů některých důležitých částí, například síťového subsystému.

Protože žádný z představených benchmarků není plně vyhovující, rozhodli jsem se implementovat vlastní program s testy přesně podle našich požadavků. Jako programovací jazyk jsme vybrali C++. Omezení na podporu překladu do nativního kódu splňují ze známých a rozšířených jazyků Pascal, C a C++, přičemž Pascal se používá především pro výukové účely. Jazyky C a C++ jsou dnes standardem pro aplikace, po kterých je vyžadován vysoký výkon. C++ jsme zvolili kvůli jeho přednostem, které zahrnující pohodlnější vývoj programů včetně možnosti objektově orientovaného programování nebo lepší typovou kontrolu. Případné přejaté knihovny mohou být i v C. Vlastní implementace benchmarku také přinese plnou kontrolu nad celým systémem, což usnadní zpracování i interpretaci naměřených výsledků.

