\chapter{Návrh aplikace}
\label{chap:treti}

V předchozí kapitole jsme odůvodnili potřebu vytvoření vlastního programu pro měření výkonu. Zásadní otázkou při tom je volba konkrétních testovacích algoritmů, čemuž jsme věnovali značnou pozornost. Všechny algoritmy jsme vybírali podle mnoha kritérií, mezi jinými i jednoduchost a přímočarost implementace, všeobecné rozšíření apod.

\section{Hlavní program}
Benchmark se skládá z několika základních funkčních bloků. Hlavním z nich je řídicí program, který zajišťuje zpracování parametrů z příkazové řádky, načtení a zpracování konfiguračního souboru, výpis výsledků, načítání a spouštění jednotlivých testů a měření doby jejich běhu. Další část tvoří kolekce všech testů seskupených do jednotlivých modulů dle cílové oblasti a typu testů. Všechny tyto moduly pak implementují předem dané rozhraní a jsou následně přeloženy jako dynamicky linkované knihovny, které jsou při spuštění řídicího programu automaticky načteny. Hlavní program může používat i některé další knihovny (například na zpracování konfiguračního souboru), které lze přilinkovat buď staticky nebo dynamicky. Základní schéma celého programu je na obr. \ref{fig:benchmark_arch}.

\begin{figure}[h]
    \centering
    \includegraphics[width=0.8\textwidth]{./img/AppSchema.pdf}
    \caption{Schéma architektury aplikace.}
    \label{fig:benchmark_arch}
\end{figure}

Tento návrh umožňuje pružně přidávat či upravovat jednotlivé moduly bez nutnosti zásahu do kódu samotného řídicího programu nebo nutnosti opětovné kompilace celého projektu.

Celý program spouští a vyhodnocuje jednotlivé testy, které byly implementovány v přidružených modulech. Jeho chování lze ovlivnit konfigurovatelnými hodnotami, které mohou být uvedeny v konfiguračním souboru nebo předány z~příkazové řádky při spuštění. Lze upravovat parametry spouštěných testů i chování řídicí aplikace. Výstupem je ke každému testu reálný čas doby běhu a čas, po který testovaný kód programu aktivně běžel na procesoru. Tento výpis bude v pevném textovém formátu, který umožňuje snadné strojové zpracování.

\section{Popis modulů}

U každého modulu nyní uvedeme seznam testů, popíšeme použité algoritmy a oblasti, kterých se testování týká. Činnost jednotlivých testů lze do značné míry ovlivnit konfigurovatelnými parametry. Jejich výchozí hodnoty jsou zvoleny tak, aby na jedné straně respektovaly hardwarové možnosti Raspberry Pi (například alokace paměti bez nutnosti využití swapovacího oddílu) a zajišťovaly na něm trvání testu alespoň v řádu vteřin a na druhé straně aby doba běhu testu na nejrychlejším z testovaných počítačů nebyla výrazně nižší než desetina vteřiny. Tento časový rozsah byl určen jako rozumný kompromis mezi přesností měření a dobou běhu celého benchmarku. Nižší dolní limit pro dobu běhu testu by znamenal zvýšenou nepřesnost měření, protože by se do výsledné hodnoty mohly ve větší míře promítnout časy některých vnitřních procesů systému, například přepínání kontextu procesoru či velikost přidělovaných časových kvant jednotlivým úlohám.

\subsection{Aplikační testy}

Aplikační testy obsahují implementace známých a často využívaných algoritmů. Tím se snaží simulovat běžnou zátěž na systém jako celek, případně jen na některé jeho části. Tyto testy se nezabývají samostatně jednotlivými komponentami.

\subsubsection*{Kryptografické funkce}
Modul \textbf{crypt} obsahuje šifrovací algoritmus AES \cite{aes} a dva algoritmy na výpočet hashovací funkce -- SHA-256 \cite{sha256} a Scrypt \cite{scrypt}.

Kryptografické funkce mají v dnešní době poměrně široké využití. AES je pravděpodobně nejpoužívanější symetrická bloková šifra, která má malé paměťové nároky (okolo 4 kB) a její výpočet probíhá velmi rychle. Tento algoritmus dokáže ve velké míře využít rychlých pamětí cache, které na moderních procesorech mají již dostatečnou velikost. Některé procesory mají navíc i specializované instrukce urychlující výpočet. Test je spouštěn na konfigurovatelně velkém poli vyplněném náhodnými daty.

SHA-256 je kryptografická hashovací funkce s výstupem dlouhým 256 bitů. Hlavní používané operace jsou logické funkce, bitový posun a sčítání. Při testování se výsledný hash počítá ze zadaného textu, kterým je vyplněno vstupní pole určité velikosti.

Scrypt je hashovací funkce navržená pro ukládání otisků hesel či generování šifrovacích klíčů pro symetrické šifry. Algoritmus byl záměrně navržen jako výpočetně pomalý převážně z důvodu vysoké paměťové náročnosti. Tento přístup má prakticky znesnadnit útoky hrubou silou.

\subsubsection*{Grafové algoritmy}
V modulu \textbf{graph} nalezneme implementaci Dijkstrova algoritmu \cite{dijkstra} pro hledání nejkratší cesty v kladně ohodnoceném grafu. Obsažená implementace používá k získání dosud nenavštíveného vrcholu s nejmenší vzdáleností od počátku 2-regulární haldu, tedy je vhodná především pro řídké grafy. Časová složitost algoritmu je pak $O((\left|V\right| + \left|E\right|) \log \left|V\right|)$. Struktura vstupního grafu je znázorněna na obr. \ref{fig:graph_struct}, platí v něm $\left|E\right| = O\left(\left|V\right|\right)$ a jako každý rovinný graf jej lze považovat za řídký. Ohodnocení hran je generováno náhodně. Konfigurovat je možno velikost grafu, tj. počet vrcholů na straně čtvercové struktury. Počátek a konec hledané cesty jsou zvoleny v pevných pozicích vzhledem k protilehlým rohům čtvercové sítě (jsou to jediné dva vrcholy grafu, které mají právě čtyři sousedy stupně čtyři), viz obrázek.

\begin{figure}[h]
    \centering
    \includegraphics[width=0.45\textwidth]{./img/graph_image.pdf}
    \caption{Struktura vstupního grafu velikosti 7. Všechny hrany jsou orientovány oběma směry, počáteční vrchol je označen zeleně, cílový červeně.}
    \label{fig:graph_struct}
\end{figure}

\subsubsection*{Databázové algoritmy}
V modulu \textbf{join} jsou implementovány dva databázové algoritmy na výpočet průniku dvou množin: hash join a merge join. Oba jsou implementovány ve dvou verzích: pouze 32bitové klíče a 32bitové klíče s 32bitovými hodnotami. Vstupní množiny jsou generovány náhodně. Hash join si nejprve vytvoří hashovací tabulku z menší množiny a poté pro každý prvek z druhé množiny hledá odpovídající klíč. Merge join nejprve obě množiny setřídí a poté při sekvenčním průchodu obou častí vybírá záznamy se stejným klíčem. Počty prvků obou množin lze dle potřeby upravovat.

\subsubsection*{Dynamické programování}
Obsahem tohoto modulu je jeden test z oblasti dynamického programování, Wag\-ner-Fischerův algoritmus \cite{wagnerfischer} na výpočet Levenshteinovy editační vzdálenosti \cite{levenshtein} dvou zadaných řetězců. V tomto testu je použit základní iterativní postup s plnou velikostí matice z důvodu vyšší paměťové náročnosti než má optimalizovaná verze algoritmu uchovávající vždy jen předchozí a aktuální řádek matice. Přístup do paměti je hlavní operací algoritmu, přičtení jedničky k celému číslu či nalezení minima ze tří čísel není výpočetně náročné.

\subsubsection*{Výpočty s reálnými čísly}
Modul \textbf{matrix} obsahuje testy na rychlost zpracování operací s čísly s plovoucí desetinnou čárkou. Jsou použity dva algoritmy na násobení čtvercových matic řádu $n$, klasické násobení s časovou složitostí $O(n^3)$ a Strassenův algoritmus ($O(n^{\log_2{7}})$) \cite{strassen}. Ten je založen na celkovém snížení počtu násobení matic za cenu většího počtu jejich rychlejšího sčítání. Výhody tohoto algoritmu se projeví až u~matic vyšších řádů. 

Každý z testů je prováděn ve dvou verzích, které se liší použitými typy proměnných -- \texttt{float} a \texttt{double}. Oba algoritmy jsou spouštěny na maticích se stejnými počátečními náhodnými daty. Jejich prvky jsou vhodně generovány tak, aby čísla ve výsledné matici zůstala mimo nestandardní hodnoty (například \texttt{+INF} či \texttt{NaN}).

\subsubsection*{Třídící algoritmy}
Modul \textbf{sort} obsahuje jediný test, algoritmus Quicksort implementovaný v standardní knihovně jazyka C++ (\texttt{std::sort}). Jedná se o jeden z nejrozšířenějších a nejčastěji používaných třídících algoritmů teoreticky i prakticky, v průměrném případě má asymptotickou časovou složitost $\varTheta(n \log n)$. Výhodou je třídění prvků bez další pomocné paměti přímo ve vstupním poli. Algoritmus je spuštěn na náhodně vygenerovaných datech (typu \texttt{int}).

Hlavní činnost tohoto algoritmu je porovnávání a prohazování položek v operační paměti. Porovnávání dvou celých čísel je na dnešních procesorech velmi rychlá operace, přístup do paměti je výrazně pomalejší, ale do jisté míry se zde také uplatní procesorové paměti cache.

\subsubsection*{Komprese dat}
Komprese je hojně využívaná technika pro snížení velikosti informací. Z dostupných algoritmů byl vybrán DEFLATE \cite{deflate}, který je složením algoritmu LZ77 \cite{lz77} s Huffmanovým kódováním \cite{huffmann} a svým použitím patří mezi nejrozšířenější. Použitá implementace pochází z open-source knihovny zlib \cite{zlib}.

Tato knihovna je velmi rozšířená, přenosná mezi různými platformami a mimo jiné také umožňuje konfiguraci velikosti používané operační paměti, takže ji lze úspěšně používat i na malých zařízeních s omezenými systémovými prostředky. Test probíhá tak, že vstupní text je nakopírován několikrát za sebe do připraveného pole, náhodně jsou změněna některá písmena a toto pole je komprimováno a následně dekomprimováno.

\subsection{Syntetické testy}

Jako syntetické označujeme ty testy, které měří parametry některé z komponent počítače, tedy jsou to spíše jednoúčelové a úzce zaměřené části programového kódu. Většina z nich by se dala zařadit do kategorie mikrobenchmark či I/O benchmark.

\subsubsection*{Paměťové testy}
Modul \textbf{mem} testuje rychlost sekvenčního čtení a zápisu do operační paměti. Oba typy testů se provádí po několika různě velkých blocích, jako hodnoty pro zápis se používají čísla, jejichž binární reprezentace pravidelně střídá bity 0 a 1 (například \texttt{0x55} pro testy s 8 bitů velkým blokem). Tyto hodnoty jsou zvoleny kvůli doplňkovému testování vadných sektorů paměti (podobná čísla používají také běžné nástroje na testování paměti, např. \textsl{badblocks}). Test probíhá v~konfigurovatelně velkém bloku paměti, jehož velikost by měla být přiměřená velikosti operační paměti Raspberry Pi.

Modul \textbf{cache} testuje vliv procesorových pamětí cache na rychlost čtení dat z~operační paměti. Obsažen je jediný test, který se využije několika způsoby podle nastavení konfiguračních parametrů.

Při testu si vyhradíme souvislý blok paměti, z kterého opakovaně čteme čtyř\-by\-to\-vé hodnoty na náhodně vybraných místech. V základním nastavení je paměťový blok dostatečně veliký, abychom při náhodném přístupu co nejčastěji požadovali data, která aktuálně nejsou uložena v paměti cache a musí se načíst přímo z operační paměti.
%Výsledek tohoto testu porovnáme s testem čtení po čtyř\-by\-to\-vých blocích z modulu \textbf{mem}, kde se naopak zásadně projeví využití pamětí cache a dopředné přednačítání dat (prefetching).
Opakovaným spouštěním testu s různou velikostí pracovního bloku dat můžeme porovnat využití paměti cache a její velikost na všech testovaných počítačích. Začneme s malým blokem, který se celý načte do paměti cache, čímž prakticky eliminujeme přímé přístupy do operační paměti. Při každém dalším běhu testu velikost bloku zdvojnásobíme, takže od některého kroku bude narůstat pravděpodobnost toho, že požadovaná data bude nutné číst přímo z operační paměti, což se projeví zpomalením testu.

Test z modulu \textbf{cache} neměří přímo propustnost paměti při náhodném přístupu k datům. Ta se z operační paměti obvykle načítají po větších blocích, jejichž velikost je rovna velikosti základního bloku paměti cache (cache line). U procesoru BCM2835 se jedná o 32 bytů, desktopové i serverové procesory Intel moderních mikroarchitektur mají 64bytové cache line. Protože nám jde pouze o relativní porovnání počítačů při stejném testu, je tento návrh měření dostačující.

\subsubsection*{Testy persistentního úložiště}
Rychlost čtecích a zapisujících operací s persistentním úložištěm, což je obvykle HDD, SSD nebo SD karta v případě jednodeskových počítačů, prověříme testy z~modulu \textbf{card}. Test zápisu je pouze sekvenční, čtení je ve dvou verzích -- sekvenční nebo s náhodným pořadím. Pro maximální rychlost je využíváno nízkoúrovňového API operačního systému místo odpovídajících prostředků standardní knihovny jazyka C++ (která navíc implementuje vyrovnávací paměť, chybovou diagnostiku apod.). Před spuštěním testů je vhodné vyprázdnit systémovou vyrovnávací paměť, aby čtení dat probíhalo přímo z testovaného úložiště a správnost výsledku nebyla negativně ovlivněna systémovými buffery.

Zápis testujeme tak, že připravené pole konfigurovatelné velikosti vyplníme náhodnými daty a opakovaně zapisujeme do souboru nastavené velikosti. Sek\-ven\-ční čtení pak čte předpřipravený soubor opět po blocích stejné velikosti. U~testu náhodného čtení lze samostatně konfigurovat velikost použitého bloku a počet iterací. Pozice ke čtení jsou generovány náhodně v odpovídajícím rozsahu k velikosti souboru.

\subsubsection*{Síťové testy}
Modul \textbf{net} slouží k měření propustnosti a latence síťového subsystému. Pro správnou funkci je nutný kompatibilní síťový odpovídací program, který je spuštěn na přímo připojeném počítači podporujícím gigabitový Ethernet. Toto zapojení proti rychlejšímu počítači umožní měřit parametry pomalejšího Raspberry Pi. Odpovídací program naslouchá na nastavené adrese a portu a všechna příchozí data ihned odešle zpět. Testy na propustnost se provádí pro protokoly TCP i UDP, \hbox{vždy} po několika velikostech bloku, latence pouze přes UDP po malých blocích, aby nedocházelo k fragmentaci do více paketů. Všechna odesílaná data jsou náhodně generovaná.

