\chapter{Měření a zpracování dat}
\label{chap:pata}
Pro měření a zhodnocení použitelnosti jednodeskového počítače Raspberry Pi jako jednotky výpočetního clusteru jsme použili dvě verze tohoto počítače, model B a vylepšený model B+. Testovaný model B je vybaven SDHC kartou značky PRETEC s kapacitou 32~GB a udávanými rychlostmi čtení a zápisu až~60 MB/s, respektive 35~MB/s. Model B+ byl testován s microSDHC kartou Kingston, jejíž kapacita podle specifikace je 32~GB, rychlost čtení až 90~MB/s a rychlost zápisu až 45~MB/s.

Pro porovnání výkonu Raspberry Pi s desktopovými a serverovými počítači jsme zvolili z obou kategorií jedno referenční zařízení. Vybraný desktopový počítač je osazen čtyřjádrovým procesorem Intel Core i7 870 mikroarchitektury Nehalem s taktovací frekvencí 2,93 GHz. Dále obsahuje 16 GB paměti RAM (typ DDR3-1600) a dvojici 100 GB SSD disků značky Kingston zapojených do RAID 1 (zrcadlení disků). Jako operační systém je použit Red Hat Enterprise Linux 7.

Vybraný server je Dell PowerEdge M910. Jedná se o NUMA (Non-Uniform Memory Access) počítač se čtyřmi osmijádrovými procesory Intel Xeon E7 4820 mikroarchitektury Nehalem s taktovací frekvencí 2 GHz doplněnými 32 GB paměti RAM (tj. 128~GB celkem). K serveru je připojeno diskové pole Infortrend ESDS 3060, které obsahuje dvojici 400 GB SSD disků sloužících jako vyrovnávací paměť a 14 kusů 4~TB HDD disků s 7200 otáčkami za minutu. Operační systém je (stejné jako v případě referenčního desktopového počítače) Red Hat Enterprise Linux verze 7.

\section{Spotřeba energie}
\label{sec:spotreba_energie}
Výrobcem udávaná spotřeba Raspberry Pi je okolo 3,5 W, respektive 2,5 W u~novějšího modelu B+. Její hodnoty závisí i na množství připojených periferií, proto ji budeme ověřovat v konfiguracích podobných podmínkám jednotlivých zařízení uvnitř výpočetního clusteru -- a aktivním síťovým připojením, bez monitoru, klávesnice a jiných periferií.

Příkon\footnote{Příkon udává množství energie spotřebované za jednotku času.} Raspberry Pi budeme měřit na napájecím kabelu mezi zdrojem a počítačem. Běžně rozšířené přístroje na měření spotřeby elektrické energie nemají dostatečnou přesnost pro rozlišení malých odběrů, proto zde provádíme měření hodnot stejnosměrného napětí a proudu a výsledný příkon spočítáme podle vzorce $P = U \cdot I$. Použitý měřící přístroj je KCX-017, který dle specifikace výrobce měří stejnosměrné napětí v rozsahu 3 až 7~V s přesností $\pm$ 0,04~V a proud v rozmezí 0,05 až 3,5~A s přesností $\pm$ 0,02~A. Účinnost nápájecích zdrojů použité velikosti se pohybuje okolo 80~\%, proto tuto hodnotu v celkovém příkonu také zohledníme.

Spotřeba Raspberry Pi verze B se podle zatížení pohybovala mezi 2,3 až 2,8~W (po startu při odpojené síti pouze 2,1~W). Kromě síťového připojení měla největší vliv práce s paměťovou kartou.

U modifikovaného modelu B+ i naše měření potvrdilo významnou úsporu energie. Po startu bez připojených periferií dosahovala spotřeba 1,2~W, jako nejvyšší hodnotu jsme zaznamenali 1,7~W (při běhu modulu \textbf{crypt}).

Ke každému uzlu navíc připočítáme spotřebu 0,5~W, která pokrývá odhadovanou spotřebu síťových prvků zajišťujících propojení uvnitř clusteru. V následujících úvahách budeme používat hodnotu 3,3~W pro starší model B a 2,2~W pro úspornější model B+. V obou případech je již započtena účinnost napájecího zdroje.

Spotřebu desktopového počítače kvalifkovaně odhadneme pomocí dostupných informací o podobných typech. Účelem tohoto testování je provést obecné srovnání spotřeby clusteru s desktopovým počítačem, proto nám nezáleží na přesné hodnotě příkonu konkrétního kusu. Přímé měření by neposkytlo relevantní údaje, protože spotřebu výrazně ovlivňuje další připojený hardware, zejména dvojice výkonných grafických karet, které jsou z pohledu našeho testování nevýznamné. Výsledná uvažovaná hodnota příkonu pro další zpracování je 250~W.

U serverového počítače zjistíme spotřebu podle údajů šasi (blade chassis), kde je zapojený. K výsledku musíme také amortizovat odhad spotřeby podpůrného hardware, hlavně šasi, které zajišťuje například síťové připojení nebo napájení jednotlivých blade serverů. Jedno šasi většinou obsahuje více těchto jednotek, proto je možné jeho vlastní spotřebu rozpočítat mezi jednotlivé servery. Dle údajů na indikátoru počítačové skříně se příkon jednoho serveru pohyboval okolo 275~W v klidu, se vzrůstající zátěží rostl až na 350~W. Po započtení vlivu dalšího podpůrného hardwaru budeme dále pracovat s hodnotou 500~W.

Spotřebu síťových prvků, diskového pole a chlazení zanedbáváme, protože tyto součásti bychom potřebovali u clusteru z Raspberry Pi i u blade serveru, tudíž na relativní porovnání nemají významný vliv.

\section{Měření výkonu}
Hlavním cílem této práce je odhadnout výpočetní schopnosti clusteru složeného z Raspberry Pi a porovnat je s běžnými počítači. Všechna měření byla provedena pomocí jednovláknových aplikací. Dále budeme vycházet z představy dokonale paralelizovatelné úlohy, která plně využívá všech dostupných fyzických jader systému\footnote{U procesorů Intel přitom odhlédneme od skutečnosti, že díky technologii Hyper-Threading se každé fyzické jádro jeví jako dvě jádra logická. Reálné zvýšení výkonu je zdrojem řady polemik. Optimistické odhady uvádějí v závislosti na druhu aplikace vzrůst o 15 až 30~\%, řada kritiků však poukazuje i na množství problémů, např. \url{http://en.wikipedia.org/wiki/Hyper-threading}.} a u níž nedochází k omezování výpočtu jednotlivých vláken způsobeným například vzájemnou synchronizací při přístupu ke sdíleným datům. K takovému idealizovanému systému budeme extrapolovat výsledky našich měření.

\subsection{Metodika}
\label{sec:metodika_vykon}
Výpočetní výkon budeme měřit pomocí připraveného benchmarku, který bude spuštěn na všech testovaných počítačích. Abychom mohli následně provést porovnání výsledků, budeme používat vždy stejné konfigurační parametry. S ohledem na omezené možnosti jednodeskového počítače Raspberry Pi jsme zvolili výchozí hodnoty konfigurovatelných parametrů, které byly přímo připraveny pro plné využití tohoto malého počítače bez vedlejších jevů jako je například přesun paměťových stránek mezi operační pamětí a diskem.

Čas běhu testu je částečně ovlivněn i prostředím, ve kterém je spouštěn. Proto při opakování testu se stejnými daty dostaneme mírně odlišné výsledky. Část změřené doby je využita na zpracování jiných úloh (systémových, jako například zpracování asynchronních operací spojených s obsluhou hardware, nebo uživatelských, tj. ostatních programů spuštěných uživateli systému) a přepínání kontextu mezi nimi, což ovlivňuje také obsazení procesorových cache i dalších vyrovnávacích pamětí operačního systému a tím nepřímo i čas testu.

Pro posouzení těchto (z našeho hlediska náhodných) vlivů jsme jeden vybraný test (třídění pole metodou quicksort) nechali proběhnout 1000 krát a zaznamenávali naměřené hodnoty. Histogram na obrázku \ref{fig:stat_qsort} reprezentuje rozdělení naměřených časů (interval mezi minimální a maximální hodnotou je rozčleněn na 20 dílů). Můžeme konstatovat, že rozptyl hodnot je poměrně malý a rozdělení je výrazně asymetrické. Směrem ke kratším časům je ostře ohraničeno, pro delší časy jsou četnosti na významném intervalu nízké, ale nenulové. Dokládá to i tzv. krabicový diagram (boxplot), na obrázku pod časovou osou. Vybarvený obdélník má hranice v dolním a horním kvartilu, čára uvnitř znázorňuje medián všech hodnot. Variabilita dat je vyznačena tzv. vousy (whiskers -- svislé čáry znázorňující minimum a maximum po vyřazení odlehlých hodnot), body ležící mimo jsou vykresleny jednotlivě a jsou pokládány za odlehlé hodnoty (outliers -- 1,5 násobek mezikvartilové vzdálenosti od přilehlého kvartilu). Stejné vyhodnocení hodnot procesorového času ukazuje, že rozdělení je kompaktnější (minimum zůstává prakticky stejné, výrazně však klesá množství odlehlých hodnot směrem k vyšším časům), což potvrzuje naše předpoklady. 

\begin{figure}[h]
  \centering
  \includegraphics[width=.8\textwidth]{./img/qsort.pdf}
  \caption{Statistické rozdělení naměřených hodnot třídícího tesu.}
  \label{fig:stat_qsort}
\end{figure}
\newpage
Na základě provedené analýzy budeme časy testů zpracovávat takto:
\begin{itemize}
\item Každý test spustíme 20 krát s identickou konfigurací. To nám zajistí dostatečně veliký soubor výsledných časů pro spolehlivé odfiltrování odlehlých hodnot a zároveň celé testovaní bude trvat přiměřeně dlouhou dobu.
\item Vyřadíme odlehlé hodnoty pro reálný i procesorový čas odděleně. Hodnotu prohlásíme za odlehlou, pokud je menší než $q_{25} - 1,5 \cdot \left(q_{75} - q_{25}\right)$ nebo větší než $q_{75} + 1,5 \cdot \left(q_{75} - q_{25}\right)$, kde $q_{25}$ je dolní a $q_{75}$ horní kvartil statistického souboru.
\item Ze zbylých hodnot spočteme aritmetický průměr, který v dalším porovnání použijeme jako reprezentativní hodnotu trvání testu. Pro případnou detailnější analýzu uvádíme u výsledných hodnot též směrodatnou odchylku, kterou lze použít k různým doplňkovým zpracování naměřených dat mimo rozsah této práce.\\
\end{itemize}

Síťové testy provádíme pouze u Raspberry Pi, protože ve výpočetním clusteru budou tato zařízení vzájemně propojena pomocí Ethernetu. Z tohoto důvodu mají na celkový výpočetní výkon clusteru určitý vliv latence a propustnost síťového subsystému. Srovnávaný desktopový a serverový počítač mají řádově rychlejší síťovou techniku a jejich komunikace je méně intenzivní, proto parametry síťového subsystému přímo nesouvisí s výpočetním výkonem zařízení. 

Uvedeným postupem jsme na všech čtyřech srovnávaných počítačích provedli opakované měření času běhu testů. Souhrnné tabulky se statisticky zpracovávanými daty jsou uvedeny v příloze \ref{chap:hodnoty}.

Celkově lze konstatovat, že relativní chyba naměřených časů je malá (typicky $<$ 1~\%), výjimkou jsou zvláště některé testy pro práci s persistentním úložištěm, kde má vliv i momentální stav souborového systému. Kromě úloh, kde se pracuje s periferiemi (persistentní úložiště, síťový subsystém) je celkový čas prakticky shodný s procesorovým časem, proto je jeho využití v následujícím zpracování opodstatněné.

Časy pro Raspberry Pi B a B+ můžeme v rámci statistických chyb považovat za stejné, větší odchylku u zápisu na paměťovou kartu přisuzujeme jejich hardwarové odlišnosti. Tím se při našem měření potvrdilo, že z výpočetního hlediska se jedná o dvě totožná zařízení.

\subsection{Aplikační testy}
Aplikačními testy jsme porovnali výkon počítačů při výpočtu několika vybraných běžných a často používaných algoritmů. Pro připomenutí uvedeme jejich stručný přehled spolu s hlavními konfiguračními parametry. Mezi implementované kryptografické algoritmy patří AES, který je spuštěn na datech o velikosti 8~MB, SHA-256, který počítá hash z dat dvojnásobné velikosti proti AES a Scrypt, jehož konfigurace je převzata z referenčního článku. Grafové algoritmy zastupuje Dijkstrův algoritmus na výpočet nejkratší cesty mezi dvěma vrcholy v ohodnoceném grafu, jehož vstup je rovinný graf s vrcholy uspořádanými do čtvercové mříže s hranou 512 bodů. Databázové algoritmy hash join a merge join spojují dvě množiny s velikostí okolo milionu prvků. Editační vzdálenost je počítána Wagner-Fischerovým algoritmem z řetězců o délce přibližně 3500 a 5300 znaků. Násobení reálných matic klasickým i Strassenovým algoritmem probíhá na čtvercových maticích řádu 512. Pro třídění čísel je použita vstupní množina se zhruba 3 miliony prvků. Komprimační knihovna provádí výpočet na datech o velikosti 8~MB.

Výsledky měření jsou znázorněny na obrázku \ref{fig:app_tests}. Zdrojová data reflektují energetickou náročnost dané úlohy a byla získána jako součin doby běhu testu a příkonu vztaženého na jedno fyzické jádro. Tato veličina představuje energii vynaloženou na provedení výpočtu. Hodnoty jsou vyneseny v relativním měřítku se vztažnou jednotkou Raspberry Pi model B+. Díky tomu můžeme výsledky všech aplikačních testů zobrazit v jednom grafu s možností snadného porovnání. Relativní měřítko je použito ke kompenzaci často výrazně odlišných energetických náročností jednotlivých testů, ale se zachováním poměrů mezi jednotlivými počítači u každého testu.

\begin{figure}[h]
    \centering
    \includegraphics[width=\textwidth]{./img/application_tests.pdf}
    \caption{Relativní porovnání energetické náročnosti aplikačních testů.}
    \label{fig:app_tests}
\end{figure}

Rozdíly mezi modely Raspberry Pi pramení prakticky pouze z hodnoty příkonu. Mezi procesory Intel vychází v tomto srovnání lépe serverový Xeon, spotřeba energie byla obecně méně než poloviční v porovnání s Core i7. Energetická náročnost na provedení úlohy u ARM v Raspberry Pi je většinou nižší než u Core i7, u některých testů dosti výrazně. Celkově lze říci, že úlohy náročné na paměť RAM vychází lépe na Raspberry Pi a naopak procesory Intel jsou výhodnější pro úlohy potřebující hrubý výpočetní výkon (například násobení matic).

Jedinou výraznou odchylkou mezi výsledky testů jsou výpočty v plovoucí desetinné čárce. Maticové násobení klasickým algoritmem je i na desktopu velmi výrazně úspornější než u Raspberry Pi. Použití Strassenova algoritmu se stejnými maticemi vykazuje daleko menší rozdíly mezi testovanými počítači. Změna doby běhu testu mezi klasickým a Strassenovým algoritmem u procesorů Intel není příliš významná, naproti tomu u ARM pozorujeme více než pětinásobné zrychlení. Díky FPU moderních procesorů jsou rozdíly mezi sčítáním a násobením čísel s plovoucí desetinnou čárkou malé nebo žádné\footnote{Instrukce FADD (sčítání) a FMUL (násobení) u mikroarchitektury Intel Nehalem viz \url{http://www.agner.org/optimize/instruction_tables.pdf}, str. 150, obdobné instrukce pro FPU jednotku procesoru ARM viz \url{http://infocenter.arm.com/help/topic/com.arm.doc.ddi0301h/DDI0301H_arm1176jzfs_r0p7_trm.pdf}, kapitola 21.11.}), proto zjištěné zrychlení přičítáme skutečnosti, že ve Strassenově algoritmu se úloha rekurzivně dělí na menší podúlohy, takže od jistého kroku se veškerá data vejdou do paměti cache. Použité matice řádu 512 zabírají v paměti $512^{2} \cdot 4{\rm~B}=1{\rm~MB}$ respektive dvojnásobek pro typ \texttt{double}, takže se obě zdrojové i výsledná matice najednou vejdou do L3 cache testovaných procesorů Intel. Proto se vliv těchto pamětí projeví i u klasického algoritmu, zatímco cache procesoru ARM lze využít až při úlohách menšího řádu ve Strassenově algoritmu.

\subsection{Doplňující syntetické testy}
\subsubsection{Paměť}
Rychlost přístupu do paměti (čtení instrukcí programu a zápisu i čtení dat) je faktor, který ovlivňuje dobu běhu každého programu. Testování propustnosti operační paměti při sekvenčním zápisu a čtení dat provádí testy z modulu \textbf{mem}. V~tabulce \ref{tab:mem_speed} jsou uvedeny naměřené hodnoty pro čtyři testované počítače při různě velkých blocích dat.

\begin{table}[h]
    \centering
\begin{footnotesize}
    \begin{tabular}{|l|c|c|c|c|}
        \hline
        & \textbf{Raspberry Pi B} & \textbf{Raspberry Pi B+} & \textbf{Desktop} & \textbf{Server} \\
        \hline
        %read\_1b & \;\;64,8 & \;\;58,0 & \;\;1643,9 & \;\;981,8\\
        %read\_2b & \;\;91,6 & \;\;84,1 & \;\;3210,8 & 1868,3\\
        read\_4b & 124,3 & 117,4 & \;\;5962,3 & 2555,1\\
        read\_8b & 128,5 & 128,5 & 10562,0 & 2194,1\\
        %write\_1b & \;\;88,8 & \;\;88,8 & \;\;1629,9 & 1010,5\\
        %write\_2b & 328,8 & 328,7 & \;\;5965,8 & 2621,9\\
        write\_4b & 328,8 & 328,8 & \;\;5969,9 & 2622,1\\
        write\_8b & 596,2 & 596,0 & \;\;5830,8 & 2563,2\\
        \hline
    \end{tabular}
    \caption{Operační paměť -- přenosové rychlosti v MB/s při čtení a zápisu dat po 32bitových a 64bitových blocích.}
    \label{tab:mem_speed}
\end{footnotesize}
\end{table}

Obecně můžeme konstatovat, že při větších blocích dat dosahujeme vyšší přenosové rychlosti. Rozdíl mezi testy s 4 bytovou a 8 bytovou hodnotou je v počtu prováděných strojových instrukcí, protože přístup do paměti probíhá v obou případech po blocích, jejichž velikost odpovídá velikosti cache line\footnote{základní kopírovatelná jednotka mezi hierarchickými úrovněmi pamětí}. Výrazný nárůst rychlosti některých testů s 8 bytovým blokem proto přisuzujeme právě tomuto rozdílu. Naopak zpomalení při testu čtení většího z bloků na serveru lze vysvětlit NUMA architekturou počítače. Pro podrobnější závěry by bylo potřeba provést dodatečná testování.

\subsubsection{Vyrovnávací paměť}
Významnou součástí architektury moderních procesorů, která výrazně ovlivňuje jejich výkon, jsou asociativní vyrovnávací paměti (cache). Oba použité procesory Intel mají tříúrovňovou datovou paměť cache, L1 má kapacitu 32~KB a L2 má 256~KB (pro každé jádro), L3 je společná pro všechna jádra. Její kapacita je 8~MB u Core i7 (desktop), respektive 18~MB u Xeon (server). ARM procesor v~Raspberry Pi má 16~KB cache první úrovně, L2 má kapacitu 128~KB\footnote{Je třeba poznamenat, že L2 u procesoru BCM2835 není součástí ARM jádra a přístup k ní není přímo řízen zabudovanou MMU. Jedná se o rozšíření firmy Broadcom sloužící v první řadě k podpoře vestavěného GPU. Ve výchozím nastavení současných verzí Raspbianu je podpora L2 již zapnuta, ale obecně platí, že její přínos pro aplikace není tak výrazný jako u jiných procesorů (v některých speciálních případech může být až negativní).}. 

Pro posouzení vlivu cache jsme nechali proběhnout test z modulu \textbf{cache} s~různými parametry. Postupně jsme měnili velikost zdrojového bufferu v rozsahu 2~KB až 64~MB a sledovali vliv na dobu běhu testu. Výsledky shrnuje obr. \ref{fig:cache_size}, hodnoty znázorňují relativní zpomalení vzhledem k testu s nejmenším bufferem (kdy je prakticky zaručeno, že všechna data jsou v cache nalezena).

\begin{figure}[h]
    \centering
    \includegraphics[width=0.8\textwidth]{./img/cache_size.pdf}
    \caption{Vliv paměti cache na přístup k datům. Na vodorovné ose je velikost použitého bufferu, na svislé je zobrazen čas, který je pro každý počítač relativní vůči době běhu testu s nejmenším bufferem.}
    \label{fig:cache_size}
\end{figure}

Se zvětšováním adresového rozsahu, ze kterého data čteme, se doba běhu nejprve prakticky nemění. To odpovídá situaci, kdy cache pojme veškerá potřebná data. Jakmile překročíme její velikost, roste pravděpodobnost toho, že data v~cache nebudou nalezena (cache miss) a je nutné jejich vyzvednutí přímo z operační paměti. Pro velké velikosti vstupního bufferu se pak paměť cache uplatňuje málo a doba běhu testu pak roste již jen mírně. Z bodu zlomu naměřených charakteristik můžeme potvrdit velikost cache L1 u Raspberry Pi 16~KB (vliv L2 není příliš výrazný), u desktopu (Intel Core i7) je zřejmý průdký nárůst křivky v~okolí 8~MB odpovídající velikosti L3 cache. K podobnému (byť ne tak prudkému) zpomalení dochází i u serverového procesoru. U něj jsme zaznamenali i částečné odchylky v~rozmezí od 128~KB (také rozptyl časů u opakovaných měření byl větší). Možným vysvětlením by mohl být vliv ostatních jader zpracovávajících jiné úlohy (ať již přímý využíváním stejné L3 cache nebo nepřímý díky technologii Turbo Boost, která adaptivně mění taktovací frekvenci). Bez využití výhody cache (tj. pro velké buffery) se doba běhu testu blíží dvojnásobku výchozí hodnoty.
%V případě serveru by to případně bylo vhodné ještě podpořit několika měřeními s většími buffery.

\subsubsection{Persistentní úložiště}
K uchovávání dlouhodobějších informací potřebuje každý počítač persistentní úložiště. Jeho rychlost významně ovlivňuje dobu potřebnou pro spuštění programů a hlavně pro práci s většími objemy dat. Testovaná zařízení se velmi liší v~druhu používaného úložiště, proto je nutné tento stav zohlednit.

\begin{table}[h]
    \centering
\begin{footnotesize}
    \begin{tabular}{|l|c|c|c|c|}
        \hline
        & \textbf{Raspberry Pi B} & \textbf{Raspberry Pi B+} & \textbf{Desktop} & \textbf{Server} \\
        \hline
        rnd\_read & \;\;0,7 & \;\;0,7 & \;\;\;\;9,3 & \;\;\;\;5,3\\
        seq\_read & 17,0 & 17,3 & 171,9 & 404,5\\
        seq\_write & \;\;2,4 & \;\;1,7 & \;\;58,1 & \;\;86,9\\
        \hline
    \end{tabular}
    \caption{Persistentní úložiště -- přenosové rychlosti v MB/s.}
    \label{tab:card_speed}
\end{footnotesize}
\end{table}

V tabulce \ref{tab:card_speed} vidíme dosažené přenosové rychlosti při sekvenčním i náhodném čtení a sekvenčním zápisu. Rozdíly při zápisu mezi oběma modely Raspberry Pi přisuzujeme odlišné použité paměťové kartě. Naměřené rychlosti při sekvenčních testech jsou (v případě zápisu výrazně) nižší než specifikace SD karet, limitujícím faktorem je tedy hardware mikropočítače. U desktopového počítače jsme vzhledem k instalovanému SSD disku naměřili poměrně nízké rychlosti. To může být způsobenu poměrně malým blokem čtených a zapisovaných dat (asi 49~MB), plná rychlost disku by se projevila až při několikanásobně větším objemu. Naopak rychlosti síťového diskového pole u serverového počítače jsou především v~sek\-ven\-čních testech velmi vysoké a výrazně předčily lokální SSD disk u testovaného desktopu.

%Výsledky po započtení energetické náročnosti vidíme na obr. \ref{fig:card_compare}. Zdrojová data jsou podílem příkonu jednoho fyzického jádra počítače ku naměřené rychlosti čtení a zápisu dat. Vztažení spotřeby na jedno procesorové jádro provádíme kvůli prováděnému jednovláknovému testování. V reálné situaci paralelního přístupu se mohou uplatnit technologie moderních disků jako například NCQ (Native Command Queuing -- optimalizace pořadí požadavků čtení a zápisu) pro celkové zrychlení. U serverového počítače je pak k dispozici velké množství disků v připojeném diskovém poli a při masivním přístupu ze všech procesorových jader by úzké místo bylo pravděpodobně připojení tohoto pole.
%
%\begin{figure}[h]
%    \centering
%    \includegraphics[width=0.9\textwidth]{./img/card_compare.pdf}
%    \caption{Porovnání energetické náročnosti testů persistentního úložiště na jedno procesorové jádro.}
%    \label{fig:card_compare}
%\end{figure}
%
%U testů persistentního úložiště se (zvláště pak u sekvenčních) výrazněji liší reálný a procesorový čas. V době čekání na dokončení vstupně-výstupních operací je procesor nevyužit a může případně vykonávat jiné úlohy. Z hlediska efektivity je nejvýhodnější serverový počítač s jeho diskovým polem. Naopak energeticky náročnější v těchto testech je desktopový počítač, který kromě sekvenčního zápisu dosahuje třikrát horších výsledků než Raspberry Pi model B+. Rozdíly mezi modely testovaného jednodeskového počítače jsou dány hlavně rozdílnou spotřebou energie, druh (a tím i rozdíl dosažených rychlostí) paměťové karty se zde příliš neprojeví.
%
Při stavbě počítačového clusteru z Raspberry Pi je vhodné kromě SD (respektive microSD) karet na jednotlivých uzlech použít ještě další perzistentní úložiště připojené přes síťový adaptér. Při dlouhodobém provozu by se mohlo negativně projevit opotřebení buněk karet při zápisu. V těchto případech se doporučuje provoz se systémovým oddílem v režimu pouze pro čtení, přičemž další proměnná systémová (např. logy) i uživatelská data jsou umístěna na síťovém úložišti. Výhodami jsou také odstranění duplicit u sdílených dat a možnost přímého přístupu k datům z vnějšku clusteru. Kromě toho, zápis přes síťové rozhraní je značně rychlejší (viz níže).

\subsubsection{Síťový subsystém}
Měření latence a propustnosti síťového subsystému probíhalo pouze na Raspberry Pi, protože tyto parametry jsou zajímavé z hlediska propojení těchto počítačů do výpočetního clusteru pomocí sítě. Dosažené hodnoty jsou v tabulce \ref{tab:net_values}. Uvedené názvy odpovídají popisu v sekci \ref{sec:modul_net}, tedy v případě testů propustnosti obsahují i velikost pracovního bufferu v kilobytech.

\begin{table}[h]
    \centering
\begin{footnotesize}
    \begin{tabular}{|l|c|c|}
        \hline
        & \textbf{model B} & \textbf{model B+} \\
        \hline
        %\hline
        latency & 0,561 & 0,571 \\
        %\hline
        \hline
        tcp\_2 & 34,6 & 34,1 \\
        tcp\_8 & 65,1 & 64,6 \\
        tcp\_32 & 73,0 & 72,2 \\
        udp\_2 & 27,0 & 26,9 \\
        udp\_8 & 52,0 & 51,7 \\
        udp\_32 & 71,4 & 71,4 \\
        \hline
    \end{tabular}
    \caption{Parametry síťového subsystému -- latence v milisekundách a testy propustnosti v Mbit/s.}
    \label{tab:net_values}
\end{footnotesize}
\end{table}

Oba modely Raspberry Pi opět dosáhly díky stejnému hardwaru prakticky totožných výsledků. Hodnoty latence představují horní hranici, protože z podstaty měření jsme dostali součet latence Raspberry Pi a pomocného počítače, který odpovídal na síťové dotazy. Vzhledem k výkonnějšímu hardware druhého počítače předpokládáme jeho vlastní latenci za prakticky zanedbatelnou. Při obdobném testu mezi dvěma uzly clusteru Iridis-Pi (viz \ref{sec:vyuziti_rpi}) zjistili autoři latenci uzlu 0,5~ms (jako polovinu celkové latence), což odpovídá našim výsledkům.

%Za pozornost stojí vyšší dosažená přenosová rychlost ve spojovaném režimu. To je způsobeno tím, že TCP nejprve provádí ukládání dat do vyrovnávací paměti a následně odesílá pakety, jejichž maximální velikost je dána hodnotou MTU (Maximum transmission unit) protokolu TCP. Tím je zajištěno spojování malých paketů do větších celků, které je možné efektivněji přenášet (větší množství dat má společnou hlavičku). Tento rozdíl obou protokolů se nejvíce projevuje při posílání malých bloků dat, s přibývající velikostí se postupně smazává.
Propustnost síťového adaptéru roste s velikostí paketů. Za pozornost stojí vyšší dosažená přenosová rychlost ve spojovaném režimu. Tento jev přikládáme způsobu implementace síťových protokolů v linuxovém jádru, kde TCP je optimalizován na vysokou propustnost a UDP spíše na nízkou dobu latence. Další rozdíl mezi oběma protokoly jsou poměry využití procesoru -- pro 32~KB bloky bylo průměrné vytížení procesoru 99~\% při TCP přenosu a pouze 47~\% při posílání UDP paketů (obdobně i pro ostatní velikosti bloku). Spojovaný protokol TCP je tedy náročnější na výpočetní výkon, ale v našem testu dosahoval mírně vyšších přenosových rychlostí.

Při testech provádíme střídavě posílání a přijímání dat po blocích dané velikosti (pracujeme vlastně v poloduplexním režimu). Přesto jsou naše výsledky dobře srovnatelné s jinými údaji, např. P. Vouzis naměřil na TCP pomocí nástroje \texttt{iperf}\footnote{Iperf -- The TCP/UDP Bandwidth Measurent Tool, domovská stránka projektu je na adrese \url{http://iperf.fr}} rychlost vysílání i příjmu 73,0~Mbit/s \cite{iperf}, maximální propustnost na Iridis-Pi při velkých blocích (od 100~KB) je uváděna 74,1~Mbit/s \cite{klastr-southampton}. Při stahování velkého souboru v lokální síti protokolem HTTP (pomocí programu \texttt{wget}) jsme naměřili až 71~Mbit/s. Je nutno poznamenat, že všechny uvedené přenosové rychlosti se týkají skutečně využitelných dat, režie síťových protokolů do nich není započtena. Z našich měření tedy plyne, že v reálných situacích lze využít přibližně tři čtvrtiny teoretické maximální propustnosti síťového adaptéru.


\section{Pořizovací náklady}
Každý z testovaných počítačů míří primárně do jiného segmentu trhu. Je zásadní rozdíl mezi nákupem Raspberry Pi na e-shopu a pořízením serveru u autorizovaného prodejce (navíc často v rámci kompletního řešení informačního systému a individuální cenové nabídky). Další obtíže při porovnání pořizovacích nákladů přináší také změny cen výrobků či kurzů měn. Proto je nutno chápat následující čísla pouze jako orientační.

U Raspberry Pi budeme počítat s cenou 1\,400~Kč, která zahrnuje také napájecí zdroj a paměťovou kartu, přičemž cena je shodná pro oba modely. Testovaný desktop je možné pořídit přibližně za 20\,000~Kč. U serveru budeme v dalším textu uvažovat pořizovací částku 400\,000~Kč. Ta přitom zahrnuje jednak cenu vlastního serveru, dále také část ceny skříně (blade chassis), ve které je server umístěn, a další poprodejní služby.

U zvažovaného clusteru započteme dodatečné náklady na propojení jednotlivých uzlů sítí Ethernet částkou 200~Kč na uzel. Mimo naše úvahy zůstávají další položky jako dodatečná síťová infrastruktura, disková úložiště, klimatizace, místo v serverovně a jiné, které jsou společné pro všechna testovaná zařízení.

U Raspberry Pi a desktopového počítače jsme neuvažovali cenu za jejich údržbu a opravy, jelikož neznáme poruchovost těchto zařízení. Havárie serverového počítače pokrývá pětiletá záruka Dell NBD, kdy je zajištěna oprava technikem přímo na místě do druhého pracovního dne.


\section{Celkové zhodnocení}

V předcházejících kapitolách jsme shrnuli výsledky porovnání testovaných zařízení z různých hledisek -- spotřeby, výpočetního výkonu a pořizovací ceny. Nyní provedeme srovnání na základě všech tří hlavních kritérií současně.

Z našich měření výpočetního výkonu při aplikačních testech určíme přibližný počet jednotek Raspberry Pi, které by v clusteru dosáhly srovnatelných výsledků jako referenční počítače. Toto číslo získáme jako průměr z reprezentativních hodnot jednotlivých modulů aplikačních testů. Reprezentativní hodnotou modulu rozumíme průměr počtů potřebných Raspberry Pi k zachování stejného výkonu vůči desktopu či serveru dosažených všemi obsaženými testy modulu. Následně pak můžeme porovnávat i pořizovací náklady a spotřebu elektrické energie clusteru a ostatních počítačů. 

Pokud přijmeme zjednodušující předpoklad ideální škálovatelnosti výpočetního výkonu mezi jádry procesorů i uzly clusteru, lze výkon referenčního desktopu srovnat se 120 jednotkami Raspberry Pi a výkon serveru pak odpovídá 580 jednotkám. Další údaje lze nalézt v tabulkách \ref{tab:compare_desktop} a \ref{tab:compare_server}. Zde je nutné zdůraznit, jak problematické je vyjádřit poměr výkonu jedním číslem i jaké nejistoty dosahují naše vstupní údaje pro spotřebu i cenu. Všechny tyto skutečnosti jsou podrobněji diskutovány v předchozím textu.

\begin{table}[h]
    \centering
    \begin{footnotesize}
    \begin{tabular}{|l|c|c|c|}
        \hline
        & \textbf{Jednotek} & \textbf{Cena} & \textbf{Spotřeba} \\
        \hline
        Desktop & \;\;\;\,1 & \;\;20\,000~Kč & 250~W \\
        Cluster RPi B & 120 & 192\,000~Kč & 396~W \\
        Cluster RPi B+ & 120 & 192\,000~Kč & 264~W \\
        \hline
    \end{tabular}
    \caption{Porovnání parametrů desktopového počítače a clusteru z Raspberry Pi obdobného výpočetního výkonu.}
    \label{tab:compare_desktop}
    \end{footnotesize}
\end{table}

\begin{table}[h]
    \centering
\begin{footnotesize}
    \begin{tabular}{|l|c|c|c|}
        \hline
        & \textbf{Jednotek} & \textbf{Cena} & \textbf{Spotřeba} \\
        \hline
        Server & \;\;\;\,1 & 400\,000~Kč & \;\;\,500~W \\
        Cluster RPi B & 580 & 928\,000~Kč & 1\,914~W \\
        Cluster RPi B+ & 580 & 928\,000~Kč & 1\,276~W \\
        \hline
    \end{tabular}
    \caption{Porovnání parametrů serverového počítače a clusteru z Raspberry Pi obdobného výpočetního výkonu.}
    \label{tab:compare_server}
\end{footnotesize}
\end{table}

Z uvedených tabulek vyplývá, že postavit výpočetní cluster je obecně draž\-ší než pořízení desktopového nebo serverového počítače odpovídajícího výkonu a také jeho spotřeba energie je vyšší. Nicméně výsledky některých testů vychází částečně pozitivněji z hlediska stavby clusteru. Nejlépe dopadly algoritmy z~modulů \textbf{sort} a \textbf{join}, kde je pro dosažení výkonu desktopového počítače potřeba pouze 35 jednotek Raspberry Pi, respektive 175 jednotek na dosažení srovnatelného výkonu s testovaným serverem. Naopak nejhorší výsledek jsme naměřili u~algoritmů z modulu \textbf{matrix}, kde by bylo potřeba 410, respektive 2010 počítačů Raspberry Pi.

%Naše výsledky pro Raspberry Pi model B jsme porovnali s údaji pro cluster Iridis-Pi, který byl postaven na univerzitě v Southamptonu ve Velké Británii (viz \ref{sec:vyuziti_rpi}). Provedená měření se shodují v oblasti pořizovacích nákladů a spotřeby. Nejpodstatnější údaj, totiž výpočetní výkon, přímé srovnání neumožňuje, neboť uváděné hodnoty se týkají pouze benchmarků LINPACK a HPL. Přesto však autoři Iridis-Pi zveřejnili výsledky těchto benchmarků na počítači s procesorem řady Intel Xeon a jimi dosažený výkonnostní poměr mezi Raspberry Pi a tímto počítačem řádově odpovídá poměru výkonu Raspberry Pi ku testovanému serveru dosaženém měřením v této práci. Nejde jistě o přímo srovnatelné údaje, přesto ale naznačují oprávněnost metodiky i konkrétního provedení našeho testování.
Naše výsledky pro Raspberry Pi model B jsme porovnali s údaji pro cluster Iridis-Pi, který byl postaven na univerzitě v Southamptonu ve Velké Británii (viz \ref{sec:vyuziti_rpi}). Provedená měření se shodují v oblasti pořizovacích nákladů a spotřeby. Nejpodstatnější údaj, totiž výpočetní výkon, přímé srovnání neumožňuje, neboť uváděné hodnoty se týkají pouze benchmarků LINPACK a HPL. Přesto však poměr výkonu Raspberry Pi ku testovanému serveru v této práci řádově odpovídá poměru mezi Raspberry Pi a počítačem s procesorem řady Intel Xeon, na kterém autoři Iridis-Pi provedli měření uvedenými benchmarky a jehož výsledky ve svém článku taktéž zveřejnili.

Ze získaných údajů plyne, že použití staršího modelu B oproti B+ nepřináší žádné výhody. Obecně lze říci, že stavba výpočetního clusteru se zatím nevyplatí. Největšími problémy jsou vyšší pořizovací cena i celková spotřeba proti desktopovým i serverovým počítačům. Nicméně pro vhodné typy úloh lze dosáhnout lepších výsledků ve spotřebě energie i pořizovací ceně než referenční server při zachování výpočetního výkonu. Navíc mezi další výhody clusteru patří také vysoká robustnost či snažší odvod přebytečného tepla.

%Pozitivním závěrům nahrávají dvě události z nedávné doby.
%Lepším výsledkům výpočetního clusteru nahrávají dvě události z nedávné doby.
Myšlence stavby výpočetního clusteru z důvodu úspory nákladů nahrávají dvě události z nedávné doby. První z nich je oznámení o snížení ceny Raspberry Pi modelu B+ o 10~\$, druhá (zásadnější) je uvedení nového modelu na trh. Raspberry Pi 2 model B obsahuje čtyřjádrový procesor s 1~GB operační paměti a podle předběžných měření je 3 až 6 krát výkonnější \cite{theregister} než předchozí model. Cena zůstala zachována na původní hladině a spotřeba by měla odpovídat staršímu modelu B. Podle těchto odhadů se stavba clusteru z nového modelu Raspberry Pi stává výhodnější než testované alternativy i pro obecnější úlohy. Pro přesnější závěry by však bylo nutné provést další měření nového zařízení.

