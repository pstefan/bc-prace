\chapter{Raspberry Pi}
\label{chap:prvni}
Raspberry Pi \cite{raspweb} je jednodeskový počítač velikosti odpovídající kreditní kartě. Vývoj tohoto počítače zajišťuje britská nadace Raspberry Pi Foundation. Projekt levného počítače vznikl díky snaze autorů zlepšit výuku programování na školách. Dnes má mnoho dalších využití, například jako domácí webový server, řídicí jednotka různých elektronických zařízení nebo plnohodnotný počítač pro nenáročné uživatele.

Myšlenka na vytvoření malého počítače sloužícího k výuce programování pro děti pochází z roku 2006 od zaměstnanců univerzity v Cambridge, kteří byli znepokojeni snižující se úrovní programátorských dovedností u nově nastupujících studentů. Chtěli navrhnout počítač, který by byl dostatečně levný a na kterém by si kdokoli mohl vyzkoušet programování.

Raspberry Pi se začalo vyrábět ve třech verzích -- model A, model B a samostatný výpočetní modul. Na začátku roku 2012 byl oficiálně zahájen prodej počítače Raspberry Pi model B s cenou 35~\$. Ostatní varianty se dočkaly uvedení na trh později.

Dva roky po spuštění prodeje překročil celkový počet prodaných kusů Raspberry Pi 3 miliony. V létě téhož roku byl představen nový model B+. Cenová politika zůstala stejná jako u původního modelu B. Hlavní novinkou je optimalizovaná spotřeba energie. Vzhled obou modelů je na obrázku \ref{fig:b_vs_bplus}.

\begin{figure}[h]
    \centering
    \includegraphics[width=0.8\textwidth]{./img/b_vs_bplus.jpg}
    \caption{Porovnání modelu B (vlevo) a novějšího B+ (vpravo). \textit{Zdroj: One Mans Anthology}}
    \label{fig:b_vs_bplus}
\end{figure}

Z hlediska stavby výpočetního clusteru je Raspberry Pi vhodné především díky nízké pořizovací ceně, malé spotřebě elektrické energie a zároveň dostatečnému výpočetnímu výkonu pro běh operačního systému. Výhodou jsou malé rozměry a pasivní chlazení celé desky. Další pozitivní faktory jsou aktivní vývoj a široká podpora ze strany výrobce i komunity.

\section{Technické parametry}
Specifikace modelů B revize 2.0 a B+ \cite{rasp-hardware}:
\begin{itemize}
\item procesor Broadcom BCM2835 (ARMv6) s taktem 700 MHz, GPU VideoCore IV s taktem 250 MHz,
\item 512 MB paměti DDR2 SDRAM sdílených s GPU,
\item 10/100 Mbit/s Ethernet adaptér s konektorem RJ45, vnitřně připojený přes USB,
\item konektor pro SD kartu (microSD u B+) -- persistentní úložiště,
\item HDMI (a Composite u modelu B) obrazový výstupní port,
\item dva USB 2.0 porty (čtyři u B+),
\item zvukový výstup přes 3,5 mm konektor,
\item další specifické porty pro přídavné obvody.
\end{itemize}
Počítač se napájí pomocí microUSB portu zdrojem se stejnosměrným napětím 5~V. Spotřeba elektrické energie se podle specifikace pohybuje okolo 3,5 W (závisí na připojených periferiích), u novějšího modelu je až o 1 W nižší.

Raspberry Pi Foundation vyvíjí a podporuje vývoj nového příslušenství. Mezi populární patří Camera module, přídavný fotoaparát s funkcí HD kamery pro Raspberry Pi. Velmi podobný je Pi NoIR, což je modul fotoaparátu bez infračerveného filtru umožňující noční vidění \cite{raspweb}.

Rozšiřující desky nabízí vylepšení základního modelu počítače. Obsahují další konektory, LED diody, přídavnou paměť, A/D převodníky a další. Populární jsou UniPi Board \cite{raspi-unipi} a Gertboard \cite{raspweb}.

S novým modelem B+ přišly vylepšené rozšiřující desky označované anglickým termínem HAT -- Hardware Attached on Top \cite{github-hats}. Tyto desky využívají přidaných GPIO pinů počítače k lepší spolupráci se systémem už od bootovacího procesu.

\section{Operační systémy a software}
Na oficiálních stránkách Raspbery Pi Foundation \cite{raspweb} jsou k dispozici odkazy na stažení obrazů různých systémů a distribucí. Většina z nich vznikla z běžných desktopových distribucí GNU/Linuxu. Oficiálně je doporučován Raspbian \cite{raspbian}. Jedná se o svobodný operační systém založený na Debianu optimalizovaný pro hardware Raspberry Pi. Pro měření v této práci budeme používat verzi 2014-12-24 založenou na Debianu Wheezy.

Další rozšířené linuxové systémy jsou Pidora (Fedora Remix) a Arch Linux ARM. Multimediální centra zastupují Raspbmc a OpenELEC, oba založené na XBMC open-source multimediálním přehrávači (dnes známým pod názvem Kodi).

Poslední rozšířenější systém je RISC OS, který byl již při svém vzniku v roce 1987 určen výhradně pro procesory s architekturou ARM. Mezi dalšími funkčními systémy můžeme zmínit Android, openSUSE, Slackware ARM, FreeBSD, OpenWrt, Kali Linux, Kano OS a Ark OS \cite{wiki-software}.

Někteří výrobci proprietárních aplikací uvolnili své programy pro Raspberry Pi zdarma. Součástí standardní instalace Raspbianu je také programovací jazyk Wolfram Language a matematický program Wolfram Mathematica \cite{wolfram}, aktuálně ve verzi Mathematica 10. Další podobný projekt je Minecraft Pi Edition \cite{minecraft}.

Od raného stádia vývoje počítače Raspberry Pi byl o tento projekt velký zájem. Jednalo se o první cenově dostupný plnohodnotný počítač s velmi malými rozměry, pasivním chlazením a příznivou spotřebou. Brzy se kolem něj vytvořila poměrně rozsáhlá komunita lidí. Práce těchto lidí je velmi vítána a oceňována, ať už jde o samotný vývoj softwaru či jako komunitní technická podpora \cite{raspweb}.

\section{Příklady využití}
Raspberry Pi se používá v mnoha zajímavých projektech a zařízeních. Zde uvedeme jen pár vybraných příkladů, které dostatečně demonstrují možnosti tohoto počítače.

\paragraph{Přístroj na měření radiace} Ionizující záření je pro člověka ve větší míře nebezpečné. Jeho intenzita se měří Geiger-Müllerovým počítačem. Zájem o tyto přístroje stoupl po jaderné havárii v elektrárně Fukušima I v Japonsku. Raspberry Pi je pro tento typ přístroje velmi vhodné. Je malé, levné, umožňuje připojit různá výstupní zařízení (monitor, reproduktory, vzdálený přijímač na síti), má nízkou spotřebu elektrické energie. K vytvoření je potřeba kromě počítače Raspberry Pi také přídavný modul PiGI a Geiger-Müllerova trubice \cite{radiace}.
\paragraph{Raspberry Pi ve stratosféře} Vypouštění balónů s měřícími zařízeními do vysokých výšek blíže k okraji vesmíru se stává čím dál častější. Mikropočítač Raspberry Pi zde posloužil k pořizování fotografií ve výšce necelých 40 km nad povrchem a jejich okamžitému odesílání zpět na Zemi. Nahradil Arduino Mini Pro, které přes nižší hmotnost a spotřebu energie nedokázalo komunikovat s webkamerou. Mikropočítač vydržel pracovat i při teplotách okolo --50\degree C a úspěšně přistál zpět \cite{vesmir}.
\paragraph{Meteorologické stanice} Malé meteorologické stanice jsou nedílnou součástí mnoha domácností. K Raspberry Pi lze připojit vlastní měřící modul AirPi \cite{airpi} nebo některou z podporovaných konvenčních stanic \cite{meteostanice}. S připojením k internetu lze sledovat naměřené hodnoty odkudkoli. Výhody Raspberry Pi jsou malá velikost, příznivá spotřeba energie a podpora ze strany některých výrobců meteorologických stanic a vývojářů obslužného softwaru.
\paragraph{Fotografování} S Raspberry Pi lze díky přídavnému fotografickému modulu vytvářet zajímavé fotografie či videa. Populární jsou videa zachycující jedno místo v různých časech (time-lapse) a videa s efektem letící kulky (bullet time effect). Ta jsou složena z fotografií scény v jediném okamžiku z různých úhlů \cite{bullet-effect}. Tyto efekty byly poprvé ve větší míře využívány ve filmech Matrix. Raspberry Pi je vhodné pro tyto účely zejména díky nízké pořizovací ceně (je nutné mít větší počet fotografických zařízení) a schopnosti propojení (je nutné dát všem fotoaparátům povel ve stejný čas), například přes ethernetový port. Velkou výhodou je přídavný fotoaparát, který je velmi malý, má dostatečné rozlišení a je pro Raspberry Pi dobře dostupný a podporovaný.
\paragraph{Kvadrokoptéra} Zařízení schopné letu, které je poháněné čtyřmi nezávislými rotory, lze ovládat také pomocí Raspberry Pi. To spolupracovalo se zařízením Arduino, které obstarávalo ovládání periferií. Raspberry Pi  pak provádělo výpočty stabilizace v reálném čase. Tento počítač byl zvolen z důvodu možnosti připojení mobilního 3G modemu, webové kamery a schopnosti dostatečně rychle provádět výpočty nutné k řízení letu \cite{kvadrokoptera}.
\\\\
Z výše uvedených příkladů je vidět, že Raspberry Pi je osvědčené zařízení využívané především v situacích, kdy je významným faktorem nízká energetická náročnost nebo nutnost většího počtu spolupracujících jednotek. To dává dobrý příslib pro toto zařízení při použití jako uzel výpočetního clusteru, kde jsou obě tyto vlastnosti důležité. Ukázalo se, že mikropočítač dokáže pracovat i v náročnějších podmínkách, což také umožňuje jeho využití v robustních systémech, které musí pracovat v terénu.

\section{Podobná zařízení}
Dnes existuje více druhů jednodeskových počítačů, Raspberry Pi byl však první, který se dostal do podvědomí širšího okruhu lidí a stal se velmi populární. Tím výrazně podpořil celý trh s jednodeskovými počítači a mnohá zařízení mají stále Raspberry Pi jako svůj vzor, ze kterého vychází nebo se jím inspirovala. Na dnešním trhu existují mikropočítače s různým výkonem, ale také různou cenou. Raspberry Pi nepatří mezi nejvýkonnější zařízení, ale příznivým poměrem ceny a výkonu se řadí mezi nejvýhodnější. Pro srovnání jsme vybrali několik známých konkurenčních zařízení\footnote{Porovnání jednodeskových počítačů je například na anglické wikipedii (\url{http://en.wikipedia.org/wiki/Comparison\_of\_single-board\_computers}) nebo na stránkách \url{http://iqjar.com/jar/an-overview-and-comparison-of-todays-single-board-micro-computers/}.}.
\\\\
\textbf{AMD Gizmo 2} \cite{amd-gizmo} patří k hardwarově vybavenějším jednodeskovým počítačům. Použitý procesor podporuje instrukční sadu kompatibilní s x86 a deska obsahuje 1 GB paměti RAM. K dispozici je 8 portů USB, port mSATA, HDMI, 3.5 mm zvukový výstup a sada GPIO pinů. Nevýhodou je aktivní chlazení pomocí ventilátoru, vyšší pořizovací cena a zhruba třikrát vyšší spotřeba energie oproti Raspberry Pi. Pokud bychom umístili více těchto zařízení do omezeného prostoru, bylo by především nutné vyřešit problém s odvodem přebytečného tepla.
\\\\
\textbf{Arduino Uno} \cite{arduino} je malý jednodeskový počítač s mikrokontrolerem ATmega328, 2 kB SRAM a 32 kB flash pamětí. Tento hardware neumožňuje běh operačního systému, deska obsahuje pouze zavaděč pro spouštění nahraného programu. Podporovaný programovací jazyk vychází z C a C++, ale v některých ohledech se může mírně lišit. Existuje více modelů desek Arduino, ale žádná nedosahuje výpočetního výkonu Raspberry Pi. Jako uzel ve výpočetním clusteru je toto zařízení nevhodné kvůli svému nízkému výkonu, nutnosti použít specifický programovací jazyk a problematickému síťování pomocí pinů na desce.
\\\\
\textbf{Arndale Board} \cite{arndale} pohání procesor Samsung Exynos 5, který spolupracuje s 2 GB paměti RAM. Deska je osazena některými nadstandardními komponenty, obsahuje například i port USB 3.0. Výrobce také nabízí řadu příslušenství jako 7-palcový LCD display, Wi-Fi, Bluetooth a GPS modul. Cena je výrazně vyšší než u Raspberry Pi, avšak toto zařízení by také mohlo být vhodné jako jednotka pro výpočetní cluster.
\\\\
Projekt \textbf{Banana Pi} \cite{bananapi} vznikl odštěpením od projektu Raspberry Pi. Cílem bylo poskytnout zákazníkům lepší hardware, ale zachovat kompatilibitu s původním Raspberry Pi v nejvyšší možné míře. Banana Pi obsahuje procesor ARM Cortex-A7 dual core, 1 GB RAM a gigabitový ethernetový port. Nejběžnější operační systémy jsou Debian, Raspbian, Arch Linux a Android. Cena zůstala příznivě nízká, ale je vyšší než u Raspberry Pi. To vše dělá z Banana Pi vhodného alternativního kandidáta pro stavbu clusteru.
\\\\
\textbf{BeagleBoard Black} \cite{beagleboard} má 1 GHz procesor ARM Cortex-A8, 512 MB paměti RAM a 4 GB interní flash paměti. Hlavní operační systémy jsou Ubuntu, Android a Debian. Oproti Raspberry Pi není BeagleBoard tolik rozšířený, má vyšší spotřebu energie, je o 10~\$ dražší, ale je nepatrně výkonější. Nevýhodou je pouze 4 GB vestavěné paměti bez jiné možnosti rozšíření než dalšího paměťového média zapojeného přes USB rozhraní, což z hlediska stavby výpočetního clusteru může být významné omezení.
\\\\
\textbf{Intel Edison} \cite{intel-edison} je nová vývojová platforma určená převážně pro nositelnou elektroniku, kterou představila na konferenci CES 2014 společnost Intel. Jedná se o počítač velikost SD karty, který obsahuje dvoujádrový procesor a integrovaný Wi-Fi a Bluetooth modul. Bylo oznámeno, že použitým operačním systémem bude Linux. Cluster sestavený z desek Intel Edison by mohl být velmi malý, ale pouze bezdrátová konektivita by mohla být při větším počtu zařízení úzké místo celého řešení.
\\\\
\textbf{Intel Galileo} \cite{intel-galileo} je vývojová deska, která je inspirována zařízeními Arduino. Zachovává i některá rozhraní podle desky Arduino Uno. Obsahuje jednojádrový 32bitový procesor Intel Quark SoC X1000 s instrukční sadou kompatibilní s x86, 256 MB DDR3 RAM. Firmware počítače je Yocto 1.4 Poky Linux. Pro programování se používá originální Arduino IDE. Deska nedosahuje výkonu Raspberry Pi a neumožňuje běh plnohodnotného operačního systému. Z důvodů slabšího výkonu a přibližně dvojnásobné ceně proti Raspberry Pi je toho zařízení méně vhodné pro stavbu výpočetního clusteru.
\\\\
\textbf{Parallella} \cite{parallella} je jednodeskový počítač zaměřený na paralelní výpočty. Obsahuje procesor Zynq-7000 Series Dual-core ARM A9, akcelerátor Epiphany s 16 nebo 64 jádry, 1 GB RAM, gigabitový ethernetový port, HDMI výstup a dal\-ší konektory. Dodává se s předinstalovaným Ubuntu OS. Pro optimální využití jsou doporučovány programovací jazyky C a C++. Zařízení je hardwarově lépe vybavené než Raspberry Pi, ale podle konkrétní specifikace je 3 až 6 krát draž\-ší. Parallella by mohla být vhodné alternativní zařízení pro stavbu výpočetního clusteru, vyšší cena může být vyvážena odpovídajícím výkonem.
\\\\
V této práci se budeme detailněji zabývat pouze Raspberry Pi model B revize 2.0 a model B+, ale dále navržená měření výkonu lze použít i na jiná zařízení splňující základní požadavky na běh operačního systému GNU/Linux. Z výše zmíněných by bylo vhodné otestovat zařízení Arndale Board, Banana Pi a Parallella. Měření na těchto dalších zařízeních je již mimo rozsah této práce, ale s~využitím připravených nástrojů by ho bylo možné snadno provést.

\section{Využití více Raspberry Pi}
\label{sec:vyuziti_rpi}
Myšlenka stavby clusteru, jehož jednotky tvoří Raspberry Pi, byla již několikrát realizována. Velmi často bylo cílem sestrojit levné a dostupné zařízení, které se svou architekturou přibližuje moderním superpočítačům, především pro účely výuky, studia jeho vlastností a ladění distribuovaných algoritmů. Některé projekty nalezly i praktické uplatnění, např. jako webový server s rozložením zátěže na jednotlivé uzly. Jiným cílem může být konstrukce robustního zařízení, protože velké množství malých zařízení může být v tomto ohledu výhodnější než jeden výkonný server.

Příkladem může být cluster sestavený J. Kiepertem na Boise State University z 32 jednotek \cite{klastr-kiepert}. Jeho projekt vznikl jako nástroj pro disertační práci, protože z důvodu oprav nemohl využívat univerzitní cluster. V publikaci se kromě základních výkonnostních charakteristik poměrně podrobně věnuje i mechanické konstrukci a řešení napájení a chlazení.

Na univerzitě v Southamptonu ve Velké Británii postavili v roce 2012 zařízení Iridis-Pi \cite{klastr-southampton}, které se skládá z 64 jednotek Raspberry Pi model B. Použili optimalizovaný operační systém Raspbian a podrobně popsali softwarovou přípravu jednotlivých jednotek. Benchmarkem \textsl{LINPACK} měřili výkonnost jednotlivých uzlů, celý cluster testovali pomocí \textsl{HPL} (\textsl{High Performance LINPACK}). Při řešení soustavy lineárních rovnic $Ax=b$ řádu 10\,240 dosáhli celkového výsledku 1,14 GFLOPS. Studovali výkonnostní charakteristiky v závislosti na velikosti úlohy i na počtu aktivních uzlů, věnovali se i hodnocení síťového připojení a přístupu na SD kartu. Kromě výuky vidí možné využití podobných zařízení díky své robustnosti a nízké spotřebě např. v automobilovém průmyslu či ve vojenské oblasti.

Stavba výpočetního clusteru vyžaduje určité technické znalosti a finanční prostředky, proto je vhodné mít předem podklady o předpokládaném výkonu a ekonomické stránce projektu. Je třeba vzít v úvahu mimo jiné odvod přebytečného tepla, zajištění dostatečného napájení a vhodné propojení jednotlivých zařízení. Protože nemáme k dispozici velké množství Raspberry Pi, provedeme měření výkonu a spotřeby na jednom zařízení a z nich odhadneme výsledné parametry clusteru postaveného z těchto jednotek. Získané hodnoty následně porovnáme s~údaji naměřenými na Iridis-Pi. Připravená testovací aplikace na porovnávaní výkonu počítačů je snadno přenositelná i na jiná zařízení, takže lze snadno provést měření i na ostatních jednodeskových počítačích, které jsme dříve doporučili.

