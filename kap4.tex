\chapter{Implementace a použití}
\label{chap:ctvrta}

V sekci \ref{sec:pozadavky} byly definovány naše požadavky na vhodný programovací jazyk a použitý operační systém. Pro implementaci benchmarku byl vybrán jazyk C++, přičemž přejaté zdrojové kódy jsou obvykle v jazyce C. K překladu je používána sada překladačů GCC ve verzi alespoň 4.8. Samotný překlad a linkování jsou řízeny utilitou GNU Make pomocí připraveného konfiguračního souboru \texttt{Makefile}. Cílový operační systém je GNU/Linux, proto budeme zachovávat běžné rozhraní programů pro tuto platformu, například syntax a sémantika přepínačů.

\section{Popis hlavního programu}
Celá aplikace se skládá z hlavního programu, který zajišťuje řízení celého testovacího procesu, a modulů s testy.

\subsection{Načítání modulů}
Jednotlivé moduly s testy se načítají dynamicky dle potřeby před začátkem testování. Všechny implementují stejné rozhraní, což zjednodušuje jejich použití i případné rozšiřování o další testy. Podrobnější popis rozhraní je v sekci \ref{sec:rozhrani_modulu}.

Knihovny s moduly jsou umístěny ve stejném adresáři jako hlavní program. Jsou rozlišitelné pomocí prefixu \texttt{bm\_} a přípony \texttt{.so} (např. \texttt{bm\_sort.so}). V základním nastavení se použijí všechny nalezené knihovny, případně je možné jejich seznam upravit pomocí přepínačů. Moduly jsou pak dynamicky načítány a pomocí vytvořujících funkcí (factory functions) jsou získány instance sady testů v každém modulu. Každá sada obsahuje instance třídy reprezentující jednotlivé testy, jejichž zpracování je sekvenčně prováděno řídicím programem.

\subsection{Průběh testování}
\label{sec:testovani}
Každý knihovní modul obsahuje jeden nebo více testů. Sada testů je reprezentována instancí třídy typu \texttt{test\_suite} a každý jednotlivý test instancí třídy typu \texttt{test} (podrobněji viz \ref{sec:rozhrani_modulu}).

Po zavedení knihovny se (dle nastavení přepínačů) načte seznam testů, které se postupně spouštějí. Pro každý test se provádí posloupnost následujících kroků:
\begin{enumerate}
\item Získání jména testu a jeho stručného popisu metodami \texttt{get\_name()}\\ a \texttt{get\_info()}. Tyto údaje jsou použity při výpisu výsledku měření nebo obsažených testů.
\item Příprava prostředí pro test (inicializace proměnných, vytvoření testovacích dat apod.) vykonáním metody \texttt{prepare\_environment()}. V případě výskytu chyby se testování ukončí a zpráva získaná zavoláním funkce\\ \texttt{get\_last\_error()} je vypsána na standardní výstup.
\item Začátek měření času.
\item Spuštění testu zavoláním metody \texttt{run\_test()}.
\item Konec měření času a vypsání času na výstup.
\item Kontrola správnosti výsledků zavoláním metody \texttt{check\_result()}. I v případě záporného výsledku se pokračuje dále. Případná chybová zpráva (získaná \texttt{get\_last\_error()}) je vypsána na výstup.
\item Zavolání metody \texttt{evaluate(double time)}. Tento krok se provede jen v~případě zapnutého rozšířeného výpisu a zároveň pokud kontrola výsledků proběhla úspěšně. V těle metody lze vypsat dodatečné informace k výsledku testu. Parametr s údajem doby běhu testu využije mimo jiné modul \textbf{mem} k vypsání rychlosti čtení dat z paměti.
\item Uvolnění všech použitých prostředků testu zajistí metoda\\ \texttt{clear\_environment()}.
\end{enumerate}
\subsection{Měření času}
Časový interval, po který byl spuštěn nějaký program, lze měřit několika způsoby. Reálný čas reprezentuje celkovou dobu běhu algoritmu od jeho spuštění po úspěšné ukončení. Tento údaj je velmi důležitý, protože udává dobu, za kterou program od svého startu vydá výsledek. Procesorový čas udává součet časových úseků, které procesor aktivně strávil vykonáváním programu. Přesnost tohoto údaje je na některých počítačích včetně Raspberry Pi omezená, protože procesorové hodiny mají řádově nižší rozlišovací schopnost než reálné hodiny a výsledný procesorový čas je součtem více hodnot, čímž se naměřené nepřesnosti úměrně zvětšují. Do procesorového času se nezapočítává například pasivní čekání na dokončení probíhající vstupně-výstupní operace.

Měření času se provácí pomocí funkce \texttt{clock\_gettime()}. Reálný čas měříme s parametrem \texttt{CLOCK\_MONOTONIC}. Pro měření procesorového času si musíme nejprve zjistit identifikátor procesorových hodin, které následně použijeme jako parametr pro \texttt{clock\_gettime()}. Počáteční i koncový čas se ukládají do struktury \texttt{timespec}. Ukázka kódu pro zjištění reálného a procesorového času:
\begin{lstlisting}
timespec cpu_begin, real_begin;
clockid_t clockid;

clock_getcpuclockid(0, &clockid);
clock_gettime(CLOCK_MONOTONIC, &real_begin);
clock_gettime(clockid, &cpu_begin);
\end{lstlisting}

Popsaným způsobem lze na Raspberry Pi měřit procesorový čas s rozlišením na mikrosekundy a reálný čas s rozlišením na nanosekundy. Na běžném desktopovém počítači dosáhneme rozlišení obou těchto časů v nanosekundách. Alternativní přístupy pomocí funkcí \texttt{clock()}, \texttt{getrusage()} či \texttt{gettimeofday()} takové rozlišení nemají. Funkce \texttt{getrusage()} umožňuje rozlišit čas procesoru strávený v~uživatelském prostoru od času stráveného v jádře, avšak přesnost na Raspberry Pi pouze na desítky milisekund je nedostatečná a neumožňuje její použití pro měření času v implementovaném benchmarku.

\section{Konfigurace}
Konfigurace programu je rozdělena na část týkající se hlavní aplikace a část pro moduly. Chování hlavního programu lze měnit pomocí parametrů předávaných z~příkazové řádky při jeho spuštění. Tento způsob byl zvolen kvůli své jednoduchosti, snadnému a rychlému používání, nízkému počtu konfigurovatelných parametrů a zachování zvyklostí velké části unixových programů. Popis jednotlivých parametrů je uveden v uživatelské dokumentaci v sekci \ref{sec:prepinace}.

Prametrů pro jednotlivé moduly je velké množství, proto jsou uloženy v konfiguračním souboru. Z běžných textových formátů jako je JSON, INI nebo XML jsme zvolili JSON, protože umožňuje ukládat strukturovaná data v jednoduše čitelném formátu a existují knihovny pro jazyk C++ umožňující komfortní práci s těmito daty.

JSON (JavaScript Object Notation) \cite{jsonformat} je jazykově nezávislý textový formát dat, který byl původně určen pro přenos údajů mezi servery a webovými aplikacemi. Data lze uchovávat v setříděných (pole) a nesetříděných (objekt) kolekcích. Pole mohou obsahovat hodnoty všech podporovaných typů -- čísla, textové řetězce, logické hodnoty, pole, objekty a prázdnou hodnotu (\texttt{null}). Objekty obsahují dvojice klíč-hodnota, kde klíč je textový řetězec a hodnota libovolný podporovaný typ. 

Pro práci s konfiguračním souborem byl vybrán parser \texttt{jsoncpp}\footnote{\url{http://open-source-parsers.github.io/jsoncpp-docs/doxygen/index.html}}, který je napsán v jazyce C++. O načtení konfiguračního souboru a jeho zpracování se stará hlavní program, který jednotlivým modulům předává odpovídající části konfigurace. Protože \texttt{jsoncpp} nepodporuje ověření struktury vstupního souboru pomocí JSON schématu\footnote{\url{http://json-schema.org/latest/json-schema-validation.html}}, tak si správný typ svých parametrů musí hlídat každý modul zvlášť. Ve všech modulech existuje globální třída \texttt{config}, která načte potřebné parametry a uchová je pro pozdější použití.

\section{Rozhraní modulů}
\label{sec:rozhrani_modulu}
Každý modul musí implementovat právě jednu třídu pro sadu testů (\texttt{test\_suite}) a přinejmenším jeden testovací algoritmus. Všechny testy jsou potomky třídy \texttt{test} a implementují její čistě virtuální metody. Dále v modulu musí být obsažena vytvořující funkce \texttt{create()}, která vrátí instanci třídy reprezentující sadu testů. Následuje popis jednotlivých částí.

\subsection{Třída test}
\begin{lstlisting}
class test {
public:
    virtual std::string get_name() = 0;
    virtual std::string get_info() = 0;
    virtual bool prepare_environment() = 0;
    virtual void run_test() = 0;
    virtual bool check_result() = 0;
    virtual void evaluate(double time) = 0;
    virtual std::string get_last_error() = 0;
    virtual bool clear_environment() = 0;
    virtual ~test() {}
};
\end{lstlisting}
Každý test musí být opakovatelný, tj. všechna vstupní data si musí sám připravit a také je zodpovědný za alokaci a dealokaci paměti potřebné pro svůj výpočet. Průběh testování a pořadí volání jednotlivých metod z hlavního programu je popsán v~sekci~\ref{sec:testovani}.\\

\paragraph{get\_name(), get\_info()} Tyto metody vrací název a krátký popis testu, například \uv{quicksort}, respektive \uv{sorting vector of items}.
\paragraph{prepare\_environment()} Tato metoda má za úkol připravit data pro testování. Může se jednat o alokaci paměti, nastavení náhodného generátoru čísel, vyplnění pole daty, atd. Pokud je nastaven přepínač indikující rozšířený výpis, pak tato metoda vypíše konfigurovatelné parametry s jejich aktuálními hodnotami. Návratová hodnota indikuje, zda vše proběhlo úspěšně a je možno spustit testování.
\paragraph{run\_test()} V těle této metody je implementován vlastní algoritmus, jehož doba běhu bude měřena. Kontrola výsledků se provádí až později, aby nebylo ovlivněno měření času zpracování testu.
\paragraph{check\_result()} Jestliže je to z podstaty algoritmu v testu možné a snadno proveditelné, pak tato metoda zkontroluje správnost jeho výsledku. Korektnost výsledné hodnoty dává metoda vědět prostřednictvím návratové hodnoty. Pokud některý test nemá možnost rozumně ověřit výsledek, pak bude tato metoda vždy vracet \texttt{true}.
\paragraph{evaluate(double)} Pokud test po skončení měřeného úseku získal nějaká výsledná data, která lze snadno vypsat na výstup, pak jsou zobrazena po zavolání této metody. K úpravě výsledku je možné použít vstupní parametr, který reprezentuje reálnou dobu běhu testu ve vteřinách, a zobrazit například rychlost práce algoritmu na jednotku dat.
\paragraph{get\_last\_error()} Pokud nějaká metoda neuspěje, pak nastaví chybovou zprávu, která daný problém popíše. Tato metoda vrátí poslední takto nastavenou zprávu.

Za správné zpracování chybových stavů odpovídá hlavní program, kde je jejich obsluha pečlivě otestována a nemůže být ovlivněna přídavnými moduly. Tento přístup byl zvolen místo modernějších výjimek z důvodu možnosti lepší optimalizace výsledného kódu kompilátorem.
\paragraph{clear\_environment()} Tato metoda zajistí uvolnění všech použitých prostředků. To může znamenat dealokace paměti, uzavření souboru či socketu.
\vskip 0pt plus 1fill

\subsection{Třída test\_suite}
\begin{lstlisting}
class test_suite {
public:
    std::vector<std::unique_ptr<test>> tests;
    void release();
    virtual std::string get_name() = 0;
    virtual std::string get_info() = 0;
    virtual void print_params() = 0;
    virtual ~test_suite() {}
};
\end{lstlisting}
Potomek této třídy musí být v celém modulu jedinečný. Stará se o kolekci všech testů v aktuálním modulu. Získání konkrétní instance se provede zavoláním vytvořující funkce v konkrétním modulu.\\

\paragraph{tests} Seznam všech testů v aktuálním modulu.
\paragraph{release()} Metoda k destrukci této třídy. Volána je z hlavního programu před ukončením modulu a zajišťuje správné uvolnění prostředků (vhodné provést v~kódu modulu, kde proběhla alokace).
\paragraph{get\_name(), get\_info()} Tyto metody vrátí jméno a krátký popis celého modulu, například \uv{sort}, respektive \uv{sorting data}.
\paragraph{print\_params()} Tato metoda vypíše konfigurační parametry celého modulu s~jejich hodnotami.
\subsection{Funkce pro vytvoření sady testů}
\begin{lstlisting}
typedef test_suite* create_t(bool, Json::Value&);
\end{lstlisting}

Vytvořující funkce v každém modulu musí dodržet tento typ a jméno \uv{create}. Typická deklarace této funkce v každém modulu je:
\begin{lstlisting}
extern "C" test_suite* create(bool verbose_flag, Json::Value& root);
\end{lstlisting}

Volající je odpovědný za korektní uvolnění veškerých prostředků pomocí metody \texttt{release()} po provedení všech testů.
\vspace{5 mm}
\section{Popis jednotlivých modulů}
\label{sec:popis_modulu}
Následuje popis implementačně zajímavých částí každého modulu spolu s vysvětlením jednotlivých konfiguračních parametrů. U každé položky je vždy v závorce uveden typ a výchozí hodnota. Používají se konstanty dvou typů -- \textsl{UInt} a \textsl{String}. V konfiguračním modulu má každý modul vlastní část, která je uvozena jeho jménem a uvnitř obsahuje všechny parametry daného modulu. Ukázka konfigurace modulu \textbf{graph}:
\begin{lstlisting}
"graph" : {
    "side" : 512,
    "seed" : 42
}
\end{lstlisting}

Výchozí hodnoty parametrů byly experimentálně zvoleny tak, aby respektovaly hardwarová omezení Raspberry Pi a zároveň zajistily dostatečně dlouhou dobu běhu testu na všech testovaných počítačích.

Algoritmický návrh a základní popis testů je uveden v kapitole \ref{chap:treti}.
%---Cache---
\subsection{Modul cache}
Tento modul testuje vliv procesorových pamětí cache na rychlost čtení dat z operační paměti. Obsahuje jediný test, jehož jméno ve výstupu programu je \texttt{cache}.

K rychlému generování náhodných indexů použijeme multiplikativní kongruenční metodu. Zvolíme $x_0 = 1$ a další čísla počítáme podle vzorce $x_{n+1} = p \cdot x_n \mod m$, kde $x_n$ je předcházející vygenerované číslo, $m$ je počet čtyřbytových čísel v bloku paměti a $p$ je prvočíslo větší než $m$. Pak dostáváme pseudonáhodná čísla z intervalu $(0;m)$. Pro účely testu byla zvolena hodnota $p = 179425907$, což je prvočíslo větší než počet čtyřbytových hodnot v celé dostupné paměti RAM u~Raspberry Pi.

\subsubsection{Parametry}
\begin{longtable}{l p{12.1cm}}
\textit{buffer\_size}&(\textsl{UInt}, $134\,217\,728$) velikost bufferu v paměti (v bytech, tj. 128~MB)\\
\textit{iterations}&(\textsl{UInt}, $13\,421\,772$) počet prováděných čtení čtyřbytových hodnot
\end{longtable}

%---Card---
\subsection{Modul card}
Modul \textbf{card} testuje rychlost čtecích a zapisujících operací s persistentním ú\-lo\-žiš\-těm. Obsahuje tři testy, které se jmenují \texttt{seq\_write}, \texttt{seq\_read} a \texttt{rnd\_read}.

Pro práci se soubory se z výkonnostních důvodů používají linuxové níz\-ko\-ú\-rov\-ňo\-vé funkce \texttt{open()}, \texttt{read()}, \texttt{write()}, \texttt{pread()}, \texttt{close()}, nikoli knihovní implementace funkcí pro vstup a výstup jako jsou například C++ streamy. V průběhu testování se vytváří dočasný soubor, který se po dokončení testu vždy odstraní.
\subsubsection{Parametry}
\begin{longtable}{l p{10.8cm}}
\textit{buffer\_size}&(\textsl{UInt}, $51\,200$) velikost bufferu (v bytech), který se používá u~sekvenčních testů\\
\textit{repetition}&(\textsl{UInt}, $1000$) určuje kolikrát se buffer zapíše do souboru, respektive kolikrát je ze souboru načten. Velikost pomocného souboru (v bytech) je tedy\\&\centerline{\textit{buffer\_size} $\cdot$ \textit{repetition}.}\\
\textit{rand\_reads}&(\textsl{UInt}, $12\,800\,000$) počet čtení v náhodném testu\\
\textit{rnd\_read\_buf\_size}&(\textsl{UInt}, $4$) velikost bufferu (v bytech) při testu s náhodným čtením\\
\textit{seed}&(\textsl{UInt}, $42$) počáteční hodnota generátoru náhodných čísel pro zápis náhodných dat do souboru a pro pozici čtení v~náhodném testu\\
\textit{file}&(\textsl{String}, "./tmp\_card\_tests") cesta a jméno dočasného souboru používaného při testech v tomto modulu. Je třeba respektovat přístupová práva k souborovému systému.
\end{longtable}

Aby výsledky testů byly porovnatelné, je nutné, aby se vždy pracovalo se stejným objemem dat. Proto je podstatné zachovat rovnost \\\centerline{\textit{buffer\_size} $\cdot$ \textit{repetition} $=$ \textit{rand\_reads} $\cdot$ \textit{rnd\_read\_buf\_size}.}\\
 
%---Crypt---
\subsection{Modul crypt}
Tento modul obsahuje tři rozšířené kryptografické algoritmy. Jejich jména jsou \texttt{aes}, \texttt{sha256} a \texttt{scrypt}.

Ke všem testům v tomto modulu jsou použity referenční implementace algoritmů v jazyce C. Licenční ujednání je vždy součástí hlavního zdrojového souboru s implementací daného algoritmu.

Test u algoritmu AES provádí šifrování i dešifrování pro kontrolu správné činnosti, u ostatních dvou toto možné z principu není.

\subsubsection{Parametry}
\begin{longtable}{l p{10.8cm}}
\textit{aes\_buf\_size}&(\textsl{UInt}, $8\,388\,608$) velikost bufferu (v bytech) pro algoritmus AES\\
\textit{aes\_seed}&(\textsl{UInt}, $42$) semínko generátoru náhodných čísel pro přípravu náhodných vstupních dat\\
\textit{sha256\_text}&(\textsl{String}, "The quick brown fox jumps over the lazy dog.") vstupní text pro výpočet SHA-256\\
\textit{sha256\_buf\_size}&(\textsl{UInt}, $16\,777\,216$) velikost pracovního bufferu algoritmu SHA\\
\textit{sha256\_hash\_size}&(\textsl{UInt}, $32$) délka výsledného hashe v bytech. Pro zachování algoritmu SHA-256 tuto hodnotu neupravujte (délka 64 bytů odpovídá algoritmu SHA-512).\\
\textit{scrypt\_N}&(\textsl{UInt}, $16\,384$) hodnota $N$ pro algoritmus Scrypt\\
\textit{scrypt\_r}&(\textsl{UInt}, $8$) hodnota $r$ pro algoritmus Scrypt\\
\textit{scrypt\_p}&(\textsl{UInt}, $1$) hodnota $p$ pro algoritmus Scrypt\\
\textit{scrypt\_key\_size}&(\textsl{UInt}, $64$) velikost výsledného hashe algoritmu Scrypt (v bytech)\\
\textit{scrypt\_text}&(\textsl{String}, "pleaseletmein") vstupní text algoritmu Scrypt\\
\textit{scrypt\_salt}&(\textsl{String}, "SodiumChloride") pomocný text pro algoritmus Scrypt
\end{longtable}
\paragraph{Poznámka} Parametry pro algoritmus Scrypt jsou podrobně vysvětleny v referenčním článku \cite{scrypt}. Složitost procesu lze ovlivnit hodnotami \textit{N}, \textit{r} a \textit{p}. Tyto parametry ovlivňují počet prováděných iterací, paměťovou náročnost a náročnost na procesorový výkon. V této práci jsou použity doporučené hodnoty z referenčního článku.\\
\vskip 0pt plus 1fill

%---Graph---
\subsection{Modul graph}
Modul \textbf{graph} obsahuje implementaci Dijkstrova algoritmu pro hledání nejkratší cesty v grafu. Ve výstupu programu používá tento test jméno \texttt{dijk\_fast}.

Hrany vstupního grafu pro Dijkstrův algoritmus jsou náhodně ohodnoceny celými čísly z intervalu $[1,10]$. Graf je čtvercová síť s diagonálami napříč směru spojnice zdrojového a cílového vrcholu (viz obr. \ref{fig:graph_struct}). Všechny hrany jsou orientovány oběma směry, ceny mohou být rozdílné.

\subsubsection{Parametry}
\begin{longtable}{l p{13.0cm}}
\textit{side}&(\textsl{UInt}, $512$) počet vrcholů na jedné straně čtvercové mříže, ve které se vytváří vstupní graf. Celkový počet vrcholů grafu je druhá mocnina této hodnoty.\\
\textit{seed}&(\textsl{UInt}, $42$) počáteční stav generátoru náhodných čísel pro přiřazování cen hranám grafu
\end{longtable}

%---Join---
\subsection{Modul join}
Databázové algoritmy hash join a merge join jsou implementovány ve dvou verzích v modulu \textbf{join}. Jména testů ve výstupu programu jsou \texttt{hash\_1}, \texttt{hash\_2}, \texttt{merge\_1} a \texttt{merge\_2}. Verze končící jedničkou pracují s množinami obsahujícími pouze klíče, verze končící dvojkou s množinami obsahujícími dvojice klíč -- hodnota.

V algoritmu hash join je hashování obou množin prováděno prostředky jazyka C++, konkrétně použitím \texttt{std::unordered\_set}.
\subsubsection{Parametry}
\begin{longtable}{l p{12cm}}
\textit{set1\_count}&(\textsl{UInt}, $1\,300\,000$) velikost první vstupní množiny\\
\textit{set2\_count}&(\textsl{UInt}, $981\,000$) velikost druhé vstupní množiny\\
\textit{seed}&(\textsl{UInt}, $42$) počáteční stav generátoru náhodných čísel
\end{longtable}

%---Levenshtein---
\subsection{Modul levenshtein}
Tento modul obsahuje Wagner-Fischerův algoritmus dynamického programování na výpočet Levenshteinovy editační vzdálenosti dvou řetězců. Jméno testu ve výstupu programu je \texttt{levenshtein}. Z důvodu možnosti konfigurace vstupních řetězců není možné provádět kontrolu výsledků, proto lze výslednou hodnotu pro možnost kontroly vypsat.
\subsubsection{Parametry}
\begin{longtable}{l p{12cm}}
\textit{first}&(\textsl{String}, ...) první vstupní řetězec\\
\textit{second}&(\textsl{String}, ...) druhý vstupní řetězec
\end{longtable}
\paragraph{Poznámka} Oba vstupní řetězce jsou dosti dlouhé, proto zde nejsou uvedeny výchozí hodnoty. Jejich délky jsou 3472 a 5269 znaků a vznikly zkopírováním náhodných odstavců anglicky psaného textu.\\
\vskip 0pt plus 1fill

%---Matrix---
\subsection{Modul matrix}
Modul \textbf{matrix} testuje rychlost operací s čísly s plovoucí desetinnou čárkou. Obsahuje algoritmus pro klasické násobení matic a efektivnější Strassenův algoritmus. Jména testů ve výstupu programu jsou \texttt{multiply\_4} a \texttt{strassen\_4} pro typ \texttt{float} a \texttt{multiply\_8} a \texttt{strassen\_8} pro typ \texttt{double}.

Prvky vstupních matic jsou generovány náhodně s rovnoměrným rozdělením, které je symetrické podle středu $\frac{1}{\sqrt{n}}$ (kde $n$ je velikost matice) a jehož levý okraj intervalu je desetina této hodnoty. To zajišťuje, že hodnoty ve výsledné matici budou dostatečně malé a nebude docházet k přetečení či podtečení datových typů v průběhu výpočtu. Pokud by se všechny prvky obou matic rovnaly střední hodnotě výše uvedeného rozdělení, pak by prvky výsledné matice měly hodnotu $n \cdot \frac{1}{\sqrt{n}} \cdot \frac{1}{\sqrt{n}} = 1$. Vstupní matice pro testy prováděné se stejným datovým typem jsou kvůli porovnání algoritmů identické. 
\subsubsection{Parametry}
\begin{longtable}{l p{13.0cm}}
\textit{size}&(\textsl{UInt}, $512$) řád vstupních i výstupních matic (matice jsou čtvercové, tedy ve výchozím stavu $512 \times 512$)\\
\textit{seed}&(\textsl{UInt}, $42$) počáteční stav generátoru náhodných čísel
\end{longtable}

%---Mem---
\subsection{Modul mem}
Testy paměti RAM (rychlost sekvenčního čtení a zápisu) probíhají v nastavitelných blocích. Čtení i zápis jsou prováděny ve verzích s velikostmi bloků 1 byte (\texttt{uint8\_t}), 2 byty (\texttt{uint16\_t}), 4 byty (\texttt{uint32\_t}) a 8 bytů (\texttt{uint64\_t}). Hodnoty, které se zapisují jsou vytvořeny z bytů \texttt{0x55} (\texttt{0x55}, \texttt{0x5555}, \texttt{0x55555555}, \dots), aby se pravidelně střídaly bity 0 a 1. Po ukončení měřené části testu se provádí kontrola těchto hodnot v paměti. V případě jejího neúspěchu je podezření na přítomnost vadných bloků v operační paměti a doporučuje se provést další testování odpovídajícími programy.

Jména testů ve výstupu programu jsou \texttt{read\_Xb} a \texttt{write\_Xb}, kde \texttt{X} označuje velikost aktuálně používaného bloku.
\subsubsection{Parametry}
\begin{longtable}{l p{11.6cm}}
\textit{memory\_size}&(\textsl{UInt}, $134\,217\,728$) velikost využité paměti v každém testu (v bytech, tj. 128 MB). Pro porovnání testů s různě velkým blokem je vhodné, aby všechny testy pracovaly s přesně stejným kusem paměti. Proto by velikost využité paměti měla být násobek velikosti největšího bloku, tj. 8 bytů.\\
\textit{read\_loop}&(\textsl{UInt}, $6$) počet opakovaného čtení celého paměťového bloku ve všech čtecích testech\\
\textit{write\_loop}&(\textsl{UInt}, $6$) počet opakovaného zápisu celého paměťového bloku ve všech zapisovacích testech
\end{longtable}

%---Net---
\subsection{Modul net}
\label{sec:modul_net}
Modul \textbf{net} slouží k měření propustnosti a latence síťového subsystému. Testy propustnosti probíhají po protokolech TCP a UDP s několika velikostmi přenášeného bloku. Tomu také odpovídají jména jednotlivých testů, která jsou \texttt{latency}, \texttt{tcp\_2}, \texttt{udp\_2}, \texttt{tcp\_8}, \texttt{udp\_8}, \texttt{tcp\_32} a \texttt{udp\_32}.

Všechny testy v tomto modulu probíhají na náhodných datech (generovaných \texttt{std::minstd\_rand0}). Latence se měří pouze na UDP s malými bloky (které nebudou při přenosu fragmentovány), výsledek je průměrná doba odpovědi na paket. V~testu nevyužíváme standardní ICMP datagramy jako u programu \texttt{ping}, protože pro práci s nimi je nutné administrátorské oprávnění a navíc mají přednostní zpracování v TCP/IP stacku, tudíž jejich použití pro měření parametrů síťového subsystému by mohlo dát zkreslené výsledky.

Ostatní testy probíhají po různě velkých blocích (2~KB, 8~KB a 32~KB), vždy po TCP i UDP. Celková velikost přenášených dat je jeden z konfigurovatelných parametrů. Výsledkem těchto testů je rychlost přenosu dat v~Mbit/s (vypsaná při zapnutém rozšířeném výstupu). Samotné testy probíhají tak, že se odešle buffer nastavené velikosti přes síťový adaptér na pomocný počítač s běžícím odpovídačem \texttt{net-echo}. Ten obratem data pošle zpět a testovaný počítač data přijme. Tento postup se opakuje do vyčerpání vstupních dat. Po dokončení testu se provádí kontrola, zda přijatá data odpovídají odeslaným.

Předpokládá se, že počítač s odpovídačem je výrazně rychlejší než testovaný stroj a je připojen přímo. Za těchto podmínek je výsledek měření latence a propustnosti ovlivněn druhým zařízením jen ve velmi malé míře.
\subsubsection{Parametry}
\begin{longtable}{l p{11.9cm}}
\textit{test\_size}&(\textsl{UInt}, $8\,388\,608$) celková velikost odesílaných dat v testech na propustnost (v bytech)\\
\textit{ping\_count}&(\textsl{UInt}, $4\,096$) počet odeslaných paketů při měření latence\\
\textit{ping\_data}&(\textsl{UInt}, $64$) velikost bloku dat posílaných při měření latence (v bytech). Tato velikost by měla být dostatečně malá, aby nedocházelo k rozdělení dat do více paketů.\\
\textit{server}&(\textsl{String}, "localhost") doménové jméno či IPv4 nebo IPv6 adresa vzdáleného počítače, proti kterému je prováděn test\\
\textit{tcp\_port}&(\textsl{UInt}, $10\,000$) TCP port na vzdáleném počítači\\
\textit{udp\_port}&(\textsl{UInt}, $10\,000$) UDP port na vzdáleném počítači\\
\textit{seed}&(\textsl{UInt}, $42$) počáteční stav generátoru náhodných čísel
\end{longtable}
\paragraph{Poznámka} Raspbian má v továrním nastavení podporu IPv6 z důvodu úspory paměti vypnutou, lze ji však snadnou zapnout. Implementované testy podporují IPv4 i IPv6, ale v základním nastavení používají IPv4.\\

%---Sort---
\subsection{Modul sort}
V tomto modulu je použita knihovní implementace třídícího algoritmu Quicksort, \texttt{std::sort}. Jako vstup dostává pole prvků typu \texttt{int}, které jsou náhodně generované s uniformním rozdělením. Jméno testu ve výstupu programu je \texttt{quicksort}.
\subsubsection{Parametry}
\begin{longtable}{l p{13cm}}
\textit{items}&(\textsl{UInt}, $3\,145\,728$) počet prvků ke třídění\\
\textit{seed}&(\textsl{UInt}, $42$) počáteční stav generátoru náhodných čísel
\end{longtable}

%---Zlib---
\subsection{Modul zlib}
Modul \textbf{zlib} obsahuje implementaci kompresního algoritmu DEFLATE z knihovny Zlib. Jméno testu ve výstupu programu je proto také \texttt{zlib}.

Zdrojové soubory komprimační knihovny jsou převzaty z referenční implementace v jazyce C. Původní text licence je součástí zdrojových souborů. Vstupní data se vytvoří opakováním konfigurovatelné znakové sekvence do zaplnění velikosti bufferu a náhodným přepisem některých písmen. Tento postup zajistí, aby algoritmus LZ77 nenahradil všechna opakování řetězce odkazem na první sekvenci, ale zároveň aby text mohl být i tímto algoritmem do jisté míry zkomprimován. Test měří čas komprese i dekomprese zároveň, kontroluje se shoda výsledku s~originálními daty.
\subsubsection{Parametry}
\begin{longtable}{l p{12.5cm}}
\textit{buf\_size}&(\textsl{UInt}, $8\,388\,608$) velikost vstupního bufferu pro komprimaci (v bytech)\\
\textit{text}&(\textsl{String}, "The quick brown fox jumps over the lazy dog.") vstupní text, pomocí kterého je vyplněn buffer ke zpracování\\
\textit{seed}&(\textsl{UInt}, $42$) počáteční stav generátoru náhodných čísel
\end{longtable}


\section{Adresářová struktura a sestavení}
Zdrojové soubory se distribuují jako archiv \texttt{benchmark.tar.gz}. Po rozbalení se v aktuálním adresáři vytvoří složka \texttt{benchmark}, jejíž obsah bude mít následující strukturu.

\begin{itemize}
\item \texttt{config.json}\, Výchozí konfigurační soubor.
\item \texttt{Makefile}\, Soubor, podle kterého se řídí vlastní překlad celé aplikace.
\item \texttt{benchmark}\, Adresář hlavního programu.
    \begin{itemize}
    \item \texttt{main.cpp}\, Zdrojový soubor hlavní aplikace.
    \end{itemize}
\item \texttt{bin}\, Adresář, do kterého se ukládají přeložené binární soubory. Pokud cílový adresář neexistuje, bude automaticky vytvořen při sestavení.
\item \texttt{cache, card, ...}\, Adresáře obsahující zdrojové kódy pro jednotlivé moduly. Produktem překladu každého z nich je sdílená knihovna, jejíž jméno vznikne doplněním prefixu \texttt{bm\_} a přípony \texttt{.so}, například \texttt{bm\_cache.so}, \texttt{bm\_card.so}.
    \begin{itemize}
    \item \texttt{main.cpp}\, Hlavní zdrojový soubor. Podle potřeby obsahují některé adresáře i další zdrojové soubory.
    \end{itemize}
\item \texttt{includes}\, Adresář pro sdílené hlavičkové soubory.
    \begin{itemize}
    \item \texttt{test.h}\, Deklarace třídy testu, sady testů a typu vytvořující funkce.
    \item \texttt{json.h}\, Hlavičkový soubor pro parser \texttt{jsoncpp}.
    \end{itemize}
\item \texttt{net-echo}\, Adresář pomocného programu pro síťové testy.
    \begin{itemize}
    \item \texttt{net-echo.cpp}\, Zdrojový soubor pomocného programu na měření síťových testů.
    \item \texttt{Makefile}\, Soubor, podle kterého se řídí překlad pomocného programu \texttt{net-echo}.
    \end{itemize}
\item \texttt{jsoncpp\_lib}\, Adresář pro zdrojové soubory parseru \texttt{jsoncpp}, který je překládán jako sdílená knihovna. Výsledný soubor má název \texttt{libjsoncpp.so} a je za překladu přilinkován k hlavnímu programu.
    \begin{itemize}
    \item \texttt{main.cpp}\, Sloučený zdrojový kód parseru podle skriptu \texttt{amalgamate.py} z repozitáře projektu\footnote{\url{https://github.com/open-source-parsers/jsoncpp}}. Odpovídající hlavičkový soubor je přesunut do složky \texttt{includes} místo původního umístění v podsložce \texttt{json}.
    \end{itemize}
\end{itemize}

Překlad aplikace včetně všech částí se řídí připraveným souborem \texttt{Makefile}. Podporovány jsou tři základní cíle -- výchozí \textit{all}, \textit{clean} a \textit{purge}. Cíl \textit {all} znamená překlad souborů, jejichž zdrojový kód se od posledního překladu změnil, nebo všech souborů, ke kterým nejsou dostupné jejich objektové ekvivalenty (soubory s~příponou \texttt{.o} v podadresářích složky \texttt{bin}). Cíl \textit{clean} smaže tyto objektové soubory a zachová přeložený hlavní program spolu se všemi moduly, kdežto cíl \textit{purge} provede smazání všech souborů vzniklých při překladu.

Vzhledem ke skutečnosti, že program je na konkrétním počítači určen spíše k~jednorázovému použití (nasbírání potřebných dat), není prováděna jeho instalace a začlenění do operačního systému, tj. nejsou podporovány cíle \textit{install} (instalace knihoven, manuálové stránky, atd.) a doplňkový \textit{uninstall}.

Pomocný program pro síťové testy se překládá pomocí vlastního souboru \texttt{Makefile} umístěného ve složce \texttt{net-echo}, kde je poté vytvořen i výsledný binární spustitelný soubor. Jediný podporovaný cíl překladu je \textit{all}.


\section{Uživatelská dokumentace}
Aplikace se spouští z příkazového řádku. Její chování závisí pouze na nastavení jednotlivých přepínačů, po spuštění již nelze průběh jakkoli ovlivnit.

\subsection{Přepínače}
\label{sec:prepinace}
Zpracování přepínačů předaných z příkazové řádky při spuštění se provádí pomocí knihovní funkce \texttt{getopt\_long()} deklarované v hlavičkovém souboru \texttt{getopt.h}. To zaručuje rozpoznávání jak krátkých (\texttt{-h}), tak i dlouhých verzí přepínačů (\texttt{-{}-help}) a také slučování krátkých verzí (například místo \texttt{-l -v} lze napsat \texttt{-lv}). Podporované přepínače jsou tyto:\\
\begin{verbatim}
benchmark [-h][-a | -i list | -e list][-v][-l][-t number][-c file]
\end{verbatim}
\begin{longtable}{l p{13.5cm}}
\texttt{-h}&(\texttt{-{}-help}) vytiskne seznam všech dostupných přepínačů a krátkou nápovědu ke každému z nich\\
\end{longtable}
Následující trojice přepínačů ovlivňuje to, které moduly budou načteny. Lze použít nejvýše jeden z nich. Pokud nebude uveden žádný, chování aplikace bude odpovídat přepínači \texttt{-a}.
\begin{longtable}{l p{13.5cm}}
\texttt{-a}&(\texttt{-{}-all}) spustí všechny testy, které najde ve stejném adresáři s hlavním programem a jejichž název odpovídá regulárnímu výrazu \texttt{bm\_[\textasciicircum \textbackslash .]+\textbackslash .so}.\\
\texttt{-i}&(\texttt{-{}-include}) spustí pouze vyjmenované testy. Přepínač má povinný argument, který obsahuje seznam jmen modulů oddělených čárkou. Například \texttt{-i mem,net,crypt}\\
\texttt{-e}&(\texttt{-{}-exclude}) spustí všechny testy kromě těch, které jsou vyjmenovány. Přepínač má povinný argument, který obsahuje seznam modulů oddělených čárkou (stejné jako u \texttt{-i}).\\
\end{longtable}
\noindent Ostatní podporované přepínače jsou uvedeny níže.
\begin{longtable}{l p{13.5cm}}
\texttt{-v}&(\texttt{-{}-verbose}) zapne podrobnější výpis. Lze použít při spuštění testů nebo při výpisu informací o modulech přepínačem \texttt{-l}.\\
\texttt{-l}&(\texttt{-{}-list}) vypíše seznam načtených modulů. S přepínačem \texttt{-v} vypíše navíc ke každému modulu seznam obsahujících testů a konfigurační parametry náležejcí k danému modulu.\\
\texttt{-t}&(\texttt{-{}-times}) určuje, kolikrát se má každý test spustit. Přepínač má povinný argument, který obsahuje celé kladné číslo.\\
\texttt{-c}&(\texttt{-{}-config}) nastaví cestu a název konfiguračního souboru. Argument přepínače je povinný, může obsahovat relativní i absolutní cestu, případně pouze jméno souboru, pokud je ve stejném adresáři s hlavním programem. Pokud přepínač není uveden, program zkusí najít soubor \texttt{config.json} v adresáři hlavního programu. V případě jeho neexistence se použijí ve všech případech výchozí hodnoty.
\end{longtable}

\subsection{Výstup}
Veškeré informace program vypisuje na standartní výstup. Je tedy možné využít různých možností příkazového řádku jako například přesměrování standardního výstupu do souboru.

Při spuštění programu je na každé řádce vypsáno jméno testu a dva časy, vždy oddělené právě jednou mezerou. První čas je reálný čas doby běhu bez ohledu na plánování vlákna na procesoru a dalších okolnostech. Druhý čas je čas skutečně využitý procesorem (CPU time). Tento čas charakterizuje využití samotného procesoru (tj. není započítána režie operačního systému, čekání na periferie apod.). Oba časy jsou uváděny v~sekundách jako desetinná čísla.

Při zapnutí rozšířeného výpisu pomocí přepínače \texttt{-v} jsou informace podrobnější, zaznamenává jednotlivé kroky, konfigurační parametry, případně vyhodnocuje výsledek měření.

Vzorový výstup pro několik kombinací přepínačů hlavního programu i pro pomocný program \texttt{net-echo} je v příloze \ref{chap:vzorove_vystupy}.

\subsection{Návratové hodnoty}
Pokud program neočekávaně skončí, vypíše na výstup důvod svého ukončení, případně lokalizaci chyby. Návratové hodnoty programu, například pro ošetření chyb ve skriptech, jsou následující:
\begin{longtable}{l p{13.2cm}}
\quad\textbf{0}&žádná chyba\\
\quad\textbf{1}&spouštěcí parametry neodpovídají specifikaci\\
\quad\textbf{2}&nepovedlo se načíst moduly testů\\
\quad\textbf{3}&nepovedlo se načíst parametry z konfiguračního souboru (pravděpodobná příčina je syntaktická chyba v souboru)\\
\quad\textbf{4}&některý parametr v konfiguračním souboru má chybný typ
\end{longtable}

\subsection{Program net-echo}
\begin{verbatim}
net-echo [-h][-t port][-u port]
\end{verbatim}
Pomocný program \texttt{net-echo} se spouští podobným způsobem jako hlavní aplikace. Podporuje tři přepínače -- \texttt{-h} (\texttt{-{}-help}), \texttt{-t} (\texttt{-{}-tcp}) a \texttt{-u} (\texttt{-{}-udp}). První zmíněný vypíše krátkou nápovědu, další dva přijímají jeden povinný parametr a to číslo příslušného portu, na kterém probíhá síťová komunikace. Pokud nejsou uvedeny, výchozí hodnota obou přepínačů je $10\,000$.

Zde bychom si dovolili připomenout, že bez administrátorských oprávnění nelze používat porty v rozsahu do 1024. Pro úspěšné navázání komunikace může být nutné na obou propojených počítačích upravit pravidla firewallu.


